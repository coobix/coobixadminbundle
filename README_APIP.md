# API PLATFORM with DDD

## Configuration

config/packages/api_platform.yaml

```yaml
api_platform:
  path_segment_name_generator: Workflow\Adapter\ApiPlatform\Operation\SingularPathSegmentNameGenerator
  mapping:
    paths: ['%kernel.project_dir%/src/Workflow/Adapter/ApiPlatform/config']
```

Workflow/Adapter/ApiPlatform/config/api_platform.yaml

```yaml
resources:
    Workflow\Domain\Model\Workflow:
        attributes:
            id:
                identifier: true

    Workflow\Domain\Model\Step:
        attributes:
            id:
                identifier: true
        operations:
            ApiPlatform\Metadata\Post:
                input: Workflow\Application\UseCase\Step\Create\DTO\CreateInputDTO
                output: Workflow\Application\UseCase\Step\Create\DTO\CreateOutputDTO
                processor: Workflow\Adapter\ApiPlatform\State\Processor\StepPostProcessor
            ApiPlatform\Metadata\Delete: ~
            ApiPlatform\Metadata\Get: ~
            ApiPlatform\Metadata\Put: ~
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)