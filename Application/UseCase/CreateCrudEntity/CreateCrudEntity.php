<?php

namespace Coobix\AdminBundle\Application\UseCase\CreateCrudEntity;

use Coobix\AdminBundle\Domain\Repository\CrudEntityRepository;

class CreateCrudEntity
{
    public function __construct(
        public readonly CrudEntityRepository $repository
    ) {
    }

    public function handle($crudEntity): void
    {
        $this->repository->save($crudEntity);
    }
}
