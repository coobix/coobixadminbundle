<?php

namespace Coobix\AdminBundle\Application\UseCase\CreateCrudEntity\DTO;

class CreateCrudEntityOutputDTO
{
    public ?string $id;
}
