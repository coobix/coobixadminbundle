<?php

namespace Coobix\AdminBundle\Application\UseCase\SearchCrudEntityByDate\DTO;

use http\Exception\InvalidArgumentException;

class SearchCrudEntityByDateInputDTO
{
    private function __construct(
        public readonly string $classFqcn
    ) {
    }

    public static function create(string $classFqcn): self
    {
        if (empty($classFqcn)) {
            throw new InvalidArgumentException('Argument $classFqcn cannot be null');
        }

        return new self($classFqcn);
    }
}
