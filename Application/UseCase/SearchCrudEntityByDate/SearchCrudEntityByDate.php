<?php

namespace Coobix\AdminBundle\Application\UseCase\SearchCrudEntityByDate;

use Coobix\AdminBundle\Application\UseCase\SearchCrudEntityByDate\DTO\SearchCrudEntityByDateInputDTO;
use Coobix\AdminBundle\Domain\Repository\CrudEntityRepository;

class SearchCrudEntityByDate
{
    public function __construct(
        public readonly CrudEntityRepository $repository
    ) {
    }

    public function handle(SearchCrudEntityByDateInputDTO $dto)
    {
        return $this->repository->searchByDate($dto->classFqcn);
    }
}
