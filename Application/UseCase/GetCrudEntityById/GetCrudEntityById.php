<?php

namespace Coobix\AdminBundle\Application\UseCase\GetCrudEntityById;

use Coobix\AdminBundle\Application\UseCase\GetCrudEntityById\Dto\GetCrudEntityByIdInputDto;
use Coobix\AdminBundle\Domain\Repository\CrudEntityRepository;

class GetCrudEntityById
{
    public function __construct(
        public readonly CrudEntityRepository $repository
    ) {
    }

    public function handle(GetCrudEntityByIdInputDto $dto)
    {
        return $this->repository->findOneById($dto->classFqcn, $dto->id);
    }
}
