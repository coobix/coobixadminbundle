<?php

namespace Coobix\AdminBundle\Application\UseCase\GetCrudEntityById\Dto;

class GetCrudEntityByIdInputDto
{
    public ?string $classFqcn = null;
    public ?string $id = null;

    public function getClassFqcn(): ?string
    {
        return $this->classFqcn;
    }

    public function setClassFqcn(?string $classFqcn): self
    {
        $this->classFqcn = $classFqcn;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public static function create(string $classFqcn, string $id): self
    {
        return (new self())
        ->setClassFqcn($classFqcn)
        ->setId($id);
    }
    
    
}
