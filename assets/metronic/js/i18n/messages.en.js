export const MESSAGES_EN = {
	confirm: 'Confirm',
	cancel: 'Cancel',
	confirm_delete_selected_items: '¿Are you sure you want to delete the selected items?',
	confirm_delete_selected: '¿Are you sure you want to delete the selected item?',
}
