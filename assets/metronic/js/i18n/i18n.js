import { MESSAGES_EN } from './messages.en'
import { MESSAGES_ES } from './messages.es'

import { VALIDATORS_EN } from './validators.en'
import { VALIDATORS_ES } from './validators.es'

export const I18N_RESOURCES = {
	resources: {
		en: {
			translation: { ...VALIDATORS_EN, ...MESSAGES_EN },
		},
		es: {
			translation: { ...VALIDATORS_ES, ...MESSAGES_ES },
		},
	},
}
