export const VALIDATORS_EN = {
	val_required: 'This value should not be blank.',
	val_one_choice: `You must select at least one choice.`,
	val_email_format: 'Incorrect email format.',
	val_min_length: `Enter at least LENGTH characters.`,
	val_max_length: `Max LENGTH characters allowed.`,
	val_pass_not_match: `Passwords does not match.`,
}
