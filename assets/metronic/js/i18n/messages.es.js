export const MESSAGES_ES = {
	confirm: 'Confirmar',
	cancel: 'Cancelar',
	confirm_delete_selected_items: '¿Desea eliminar los registros seleccionados?',
	confirm_delete_selected: '¿Desea eliminar el registro seleccionado?',
}
