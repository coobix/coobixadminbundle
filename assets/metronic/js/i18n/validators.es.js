export const VALIDATORS_ES = {
	val_required: 'Este valor no debería estar vacío.',
	val_one_choice: `Debe seleccionar al menos una opción.`,
	val_email_format: 'Format de email incorrecto.',
	val_min_length: `Ingrese al menos LENGTH caracteres.`,
	val_max_length: `Max LENGTH caracteres permitidos.`,
	val_pass_not_match: `Las contraseñas no coinciden.`,
}
