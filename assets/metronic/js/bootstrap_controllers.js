import { startStimulusApp } from '@symfony/stimulus-bridge'
import Toast_controller from 'coobix-commons/src/controllers/toast_controller'
import Spinner_controller from 'coobix-commons/src/controllers/spinner_controller'
import SubmitConfirm_controller from 'coobix-commons/src/controllers/submit-confirm_controller'
import Flatpickr_controller from "coobix-commons/src/controllers/flatpickr_controller";

// Registers Stimulus controllers from controllers.json and in the controllers/ directory

export const app_coobixadmin = startStimulusApp(
	require.context('@symfony/stimulus-bridge/lazy-controller-loader!./controllers', true, /\.[jt]sx?$/),
)
// register any custom, 3rd party controllers here
app_coobixadmin.register('toast', Toast_controller)
app_coobixadmin.register('spinner', Spinner_controller)
app_coobixadmin.register('submit-confirm', SubmitConfirm_controller)
app_coobixadmin.register('flatpickr', Flatpickr_controller)
