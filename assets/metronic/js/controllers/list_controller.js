'use strict'
import { Controller } from '@hotwired/stimulus'
import '../../styles/list.css'

/*
 * This class is responsible for handle an admin list
 * Any element with a data-controller="list" attribute will cause
 * this controller to be executed.
 */
export default class extends Controller {
	static targets = ['table', 'bulkCheckAll', 'bulkCheck', 'listBulkIdsSelectedInput']

	static values = { bulkCheckAll: Boolean, default: false }

	/**
	 * This function is responsible for update bulkCheckAllValue
	 * when the input checked/unchecked
	 * This is only for checkbox to track the value change
	 * @param e
	 */
	setBulkCheckAll(e) {
		this.bulkCheckAllValue = e.currentTarget.checked
		e.currentTarget.value = e.currentTarget.checked
	}

	/**
	 * This function is responsible for check/uncheck all checkboxes
	 * when bulkCheckAllValue change
	 */
	bulkCheckAllValueChanged() {
		//check  / uncheck all checkbox
		this.bulkCheckTargets.forEach((c) => {
			c.checked = this.bulkCheckAllValue
		})
		this.toggleToolbars()
	}

	onSingleListCheckBoxChange(e) {
		this.toggleToolbars()
	}

	/**
	 * This function is responsible for display button to delete when using list checkboxes
	 */
	toggleToolbars() {
		if (!this.hasListBulkIdsSelectedInputTarget) {
			return
		}
		const table = this.tableTarget
		//TODO use targets to get those elements
		const toolbarBase = document.querySelector('[data-kt-subscription-table-toolbar="base"]')
		const selectedCount = document.querySelector('[data-kt-subscription-table-select="selected_count"]')
		const toolbarSelected = document.querySelector('[data-kt-subscription-table-toolbar="selected"]')

		// Detect checkboxes state & count
		let checkedState = false
		let count = 0

		let listBulkIdsSelected = '0'
		// Count checked boxes
		this.bulkCheckTargets.forEach((c) => {
			if (c.checked) {
				listBulkIdsSelected += ',' + c.value
				checkedState = true
				count++
			}
		})
		//add ids to the form input to be sent
		this.listBulkIdsSelectedInputTarget.value = listBulkIdsSelected

		// Toggle toolbars
		if (checkedState) {
			selectedCount.innerHTML = count
			toolbarBase.classList.add('d-none')
			toolbarSelected.classList.remove('d-none')
		} else {
			toolbarBase.classList.remove('d-none')
			toolbarSelected.classList.add('d-none')
		}
	}
}
