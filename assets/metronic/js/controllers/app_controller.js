'use strict'
import { Controller } from '@hotwired/stimulus'

import KTMenu from 'coobix-commons/src/components/menu'
import KTDrawer from 'coobix-commons/src/components/drawer'
import i18next from 'i18next'
import { I18N_RESOURCES } from '../i18n/i18n'

/*
 * This controller is responsible for handle autocomplete widget!
 *
 * Any element with a data-controller="app" attribute will cause
 * this controller to be executed.
 */
export default class extends Controller {
	static values = {
		appLanguage: String,
	}

	connect() {
		i18next.init({
			lng: this.appLanguageValue || 'en', // if you're using a language detector, do not define the lng option
			debug: true,
			...I18N_RESOURCES,
		})

		window.KTUtilElementDataStore = window.KTUtilElementDataStore || {}
		window.KTUtilElementDataStoreID = window.KTUtilElementDataStoreID || 0
		window.KTUtilDelegatedEventHandlers = window.KTUtilDelegatedEventHandlers || {}
		window.i18next = window.i18next || i18next

		// On document ready
		if (document.readyState === 'loading') {
			document.addEventListener('DOMContentLoaded', this.initMenu)
			document.addEventListener('DOMContentLoaded', this.initDrawer)
		} else {
			this.initMenu()
			this.initDrawer()
		}
	}

	initMenu() {
		// Global Event Handlers
		KTMenu.initGlobalHandlers()
		// Lazy Initialization
		KTMenu.createInstances()
	}

	initDrawer() {
		// Global initialization
		KTDrawer.createInstances()
		KTDrawer.handleShow()
		KTDrawer.handleDismiss()
	}
}
