import { Application } from '@hotwired/stimulus'
import { clearDOM, mountDOM } from '@symfony/stimulus-testing'
import { fireEvent, getByTestId } from '@testing-library/dom'
import userEvent from '@testing-library/user-event'

// adds special assertions like toHaveTextContent
import '@testing-library/jest-dom'
import List_controller from '../../../metronic/js/controllers/list_controller'

const startStimulus = () => {
	const application = Application.start()
	application.register('list', List_controller)
}

describe('List_controller', () => {
	let container

	beforeEach(() => {
		container = mountDOM(element)
	})

	afterEach(() => {
		clearDOM()
	})

	/*
    it('connect',  () => {
        startStimulus();
    });
*/
	test('sumar 1 + 2 es igual a 3', () => {
		const application = Application.start()
		application.register('list', List_controller)
		const input = getByTestId(container, 'checkAll')
		const deleteSelectedContainer = getByTestId(container, 'deleteSelectedContainer')

		expect(deleteSelectedContainer).toHaveClass('d-none')
		fireEvent.click(input)
		input.dispatchEvent(new CustomEvent('change'))

		input.dispatchEvent(new CustomEvent('click'))
		//input.simulate;
		expect(getByTestId(container, 'deleteSelectedContainer')).toHaveClass('d-noneaa')
	})

	// You can create other tests here
})

let element =
	'<div class="card" data-controller="list">\n' +
	'\n' +
	'        <!--begin::List header-->\n' +
	'        <div class="card-header border-0 pt-6">\n' +
	'            <!--begin::Card title-->\n' +
	'            <div class="card-title">\n' +
	'                <!--begin::Search-->\n' +
	'                <div class="d-flex align-items-center position-relative my-1">\n' +
	'\n' +
	'                </div>\n' +
	'                <!--end::Search-->\n' +
	'            </div>\n' +
	'            <!--begin::Card title-->\n' +
	'\n' +
	'            <!--begin::Card toolbar-->\n' +
	'            <div class="card-toolbar">\n' +
	'                                    <!--begin::Toolbar-->\n' +
	'                    <div class="d-flex justify-content-end" data-kt-subscription-table-toolbar="base">\n' +
	'                                                    <!--begin::Filter-->\n' +
	'                            <button type="button" class="btn btn-light-primary me-3" data-bs-toggle="collapse" data-bs-target="#advancedSearch_body_1" aria-expanded="true" aria-controls="advancedSearch_body_1">\n' +
	'                            <span class="svg-icon svg-icon-2">\n' +
	'\t\t\t\t\t\t\t    <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->\n' +
	'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">\n' +
	'    <path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="black"></path>\n' +
	'</svg>\n' +
	'<!--end::Svg Icon-->\n' +
	'\t\t\t\t\t\t\t</span> Filters\n' +
	'                            </button>\n' +
	'                            <!--end::Filter-->\n' +
	'                                                <!--begin::Add subscription-->\n' +
	'                        <a href="/admin/workflow/new" class="btn btn-primary">\n' +
	'                            <span class="svg-icon svg-icon-2">\n' +
	'                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->\n' +
	'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">\n' +
	'    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>\n' +
	'    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>\n' +
	'</svg>\n' +
	'<!--end::Svg Icon-->\n' +
	'                            </span> Add</a>\n' +
	'                        <!--end::Add subscription-->\n' +
	'\n' +
	'                    </div>\n' +
	'                    <!--end::Toolbar-->\n' +
	'\n' +
	'                    <!--begin::Group actions-->\n' +
	'                                            <div class="d-flex justify-content-end align-items-center d-none" data-kt-subscription-table-toolbar="selected" data-testid="deleteSelectedContainer">\n' +
	'    <div class="fw-bolder me-5">\n' +
	'        <span class="me-2" data-kt-subscription-table-select="selected_count"></span>Selected\n' +
	'    </div>\n' +
	'    <form role="form" id="listBulkForm" action="/admin/workflow/admin_bulk" method="post" data-controller="submit-confirm" data-action="submit-confirm#onSubmit">\n' +
	'        <input type="hidden" name="action" id="action" value="delete">\n' +
	'        <input type="hidden" name="bulkIds" id="bulkIds" value="0" data-list-target="listBulkIdsSelectedInput">\n' +
	'\n' +
	'        <button type="submit" class="btn btn-danger btn-with-spinner" data-kt-subscription-table-select="delete_selected" data-submit-confirm-target="submitButton">\n' +
	'            Delete Selected\n' +
	'        </button>\n' +
	'    </form>\n' +
	'</div>                                        <!--end::Group actions-->\n' +
	'\n' +
	'                            </div>\n' +
	'            <!--end::Card toolbar-->\n' +
	'\n' +
	'        </div>\n' +
	'                    \n' +
	'\n' +
	'    <div class="accordion list-search-accordion" id="advancedSearch">\n' +
	'        <div class="accordion-item">\n' +
	'\n' +
	'            <div id="advancedSearch_body_1" class="accordion-collapse collapse display-none" data-bs-parent="#advancedSearch">\n' +
	'                <div class="accordion-body">\n' +
	'                                        <form name="list_search" method="get" action="/admin/workflow/list" class="" role="form">\n' +
	'                    \n' +
	'                    <div id="list_search"><div class="mb-3"><label for="list_search_name" class="form-label">Name</label><input type="text" id="list_search_name" name="list_search[name]" class="form-control">        </div><input type="hidden" id="list_search__token" name="list_search[_token]" value="a29bef44e83bb9a818.ESr26GCr8HSnzMVc7XOvH7qr4ZVbSHwt5rjRiDp-dEs.S2CkqiTmmEXJ_oI-qBL3RsDiqMQYfg5hh4ya4G8ZHxRbTb2RFuaYLfeV9Q"></div>\n' +
	'                    <div class="fix">\n' +
	'                        <a href="/admin/workflow/list" class="btn btn-light">RESET</a>\n' +
	'                        <button type="submit" class="btn btn-light">FILTER</button>\n' +
	'                    </div>\n' +
	'                    </form>\n' +
	'                </div>\n' +
	'            </div>\n' +
	'        </div>\n' +
	'    </div>\n' +
	'                <!--end::List header-->\n' +
	'        <!--begin::List body-->\n' +
	'        <div class="card-body pt-0">\n' +
	'            <!--begin::Table-->\n' +
	'            <div class="table-responsive">\n' +
	'                                    <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_subscriptions_table" data-list-target="table">\n' +
	'                        <!--begin::Table head-->\n' +
	'                        <thead>\n' +
	'                        <!--begin::Table row-->\n' +
	'                        <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">\n' +
	'                                                            <th class="w-10px pe-2">\n' +
	'                                    <div class="form-check form-check-sm form-check-custom form-check-solid me-3">\n' +
	'                                        <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_subscriptions_table .form-check-input" data-list-target="bulkCheckAll" data-action="list#setBulkCheckAll" value="0" data-testid="checkAll">\n' +
	'                                    </div>\n' +
	'                                </th>\n' +
	'                                                                    <th class="min-w-125px">\n' +
	'                                        <a class="sortable" href="/admin/workflow/list?sort=e.name&amp;direction=asc&amp;page=1" title="name">name</a>\n' +
	'\n' +
	'                                    </th>\n' +
	'                                                                <th class="text-end min-w-70px">Actions</th>\n' +
	'                                                    </tr>\n' +
	'                        <!--end::Table row-->\n' +
	'                        </thead>\n' +
	'                        <!--end::Table head-->\n' +
	'                        <!--begin::Table body-->\n' +
	'                        <tbody class="text-gray-600 fw-bold">\n' +
	'                                                    \n' +
	'                                                                                                                                                            <tr>\n' +
	'                                    <!--begin::Checkbox-->\n' +
	'                                    <td>\n' +
	'                                        <div class="form-check form-check-sm form-check-custom form-check-solid">\n' +
	'                                            <input name="bulkCheck" class="form-check-input" type="checkbox" value="21faac10-8bc0-4c71-b38e-128ad74f7d39" data-list-target="bulkCheck" data-action="list#onSingleListCheckBoxChange">\n' +
	'                                        </div>\n' +
	'                                    </td>\n' +
	'                                    <!--end::Checkbox-->\n' +
	'\n' +
	'                                                                            <td>\n' +
	'                                            <a href="/admin/workflow/21faac10-8bc0-4c71-b38e-128ad74f7d39/show" class="text-gray-800 text-hover-primary mb-1">\n' +
	'                                                new client\n' +
	'                                            </a>\n' +
	'                                        </td>\n' +
	'                                    \n' +
	'                                    <!--begin::Action=-->\n' +
	'                                    <td class="text-end">\n' +
	'                                        <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions\n' +
	'                                            <span class="svg-icon svg-icon-5 m-0">\n' +
	'                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->\n' +
	'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">\n' +
	'    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>\n' +
	'    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>\n' +
	'</svg>\n' +
	'<!--end::Svg Icon-->\n' +
	'\t\t\t\t\t\t\t\t\t</span>\n' +
	'                                        </a>\n' +
	'                                        <!--begin::Menu-->\n' +
	'                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/21faac10-8bc0-4c71-b38e-128ad74f7d39/show" class="menu-link px-3">View</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/21faac10-8bc0-4c71-b38e-128ad74f7d39/edit" class="menu-link px-3">Edit</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                \n' +
	'    <form action="http://127.0.0.1:8004/admin/workflow/delete" method="post" data-controller="submit-confirm" data-action="submit-confirm#onSubmit">\n' +
	'        <input type="hidden" name="token" value="a7426295b3886dfec0f7fcdf3eb6c826.37Pk_l9-liU2rQgEPh-0a1qXK2RZ5I7kGvNwf9UbggA.vP6rnWkQ1HFnxGBDd1bTKm7SSl0PhcCQU5VELKZS71Sd8qiJNDL-Q07iPA">\n' +
	'        <input type="hidden" name="id" value="21faac10-8bc0-4c71-b38e-128ad74f7d39">\n' +
	'\n' +
	'        <button type="submit" id="form_21faac10-8bc0-4c71-b38e-128ad74f7d39_submit" class="menu-link px-3 list-delete-form-button " data-submit-confirm-target="submitButton">Delete\n' +
	'        </button>\n' +
	'    </form>\n' +
	'\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                        </div>\n' +
	'                                        <!--end::Menu-->\n' +
	'                                    </td>\n' +
	'                                    <!--end::Action=-->\n' +
	'                                </tr>\n' +
	'                                                                                                                                                            <tr>\n' +
	'                                    <!--begin::Checkbox-->\n' +
	'                                    <td>\n' +
	'                                        <div class="form-check form-check-sm form-check-custom form-check-solid">\n' +
	'                                            <input name="bulkCheck" class="form-check-input" type="checkbox" value="73281a40-9472-461d-bd5f-75c65d4c83ab" data-list-target="bulkCheck" data-action="list#onSingleListCheckBoxChange">\n' +
	'                                        </div>\n' +
	'                                    </td>\n' +
	'                                    <!--end::Checkbox-->\n' +
	'\n' +
	'                                                                            <td>\n' +
	'                                            <a href="/admin/workflow/73281a40-9472-461d-bd5f-75c65d4c83ab/show" class="text-gray-800 text-hover-primary mb-1">\n' +
	'                                                test\n' +
	'                                            </a>\n' +
	'                                        </td>\n' +
	'                                    \n' +
	'                                    <!--begin::Action=-->\n' +
	'                                    <td class="text-end">\n' +
	'                                        <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions\n' +
	'                                            <span class="svg-icon svg-icon-5 m-0">\n' +
	'                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->\n' +
	'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">\n' +
	'    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>\n' +
	'    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>\n' +
	'</svg>\n' +
	'<!--end::Svg Icon-->\n' +
	'\t\t\t\t\t\t\t\t\t</span>\n' +
	'                                        </a>\n' +
	'                                        <!--begin::Menu-->\n' +
	'                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/73281a40-9472-461d-bd5f-75c65d4c83ab/show" class="menu-link px-3">View</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/73281a40-9472-461d-bd5f-75c65d4c83ab/edit" class="menu-link px-3">Edit</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                \n' +
	'    <form action="http://127.0.0.1:8004/admin/workflow/delete" method="post" data-controller="submit-confirm" data-action="submit-confirm#onSubmit">\n' +
	'        <input type="hidden" name="token" value="8ffdf4908c87d.LBkmudqdDzgxVBb7BCpqcNuVdQZCEVXM-WKf4IxaZQg.T1Rp2uzzTWxgPX68TWMNMe_QFD8UcBu4sASrs_8TCFxuWGrOsdFnXkkbIg">\n' +
	'        <input type="hidden" name="id" value="73281a40-9472-461d-bd5f-75c65d4c83ab">\n' +
	'\n' +
	'        <button type="submit" id="form_73281a40-9472-461d-bd5f-75c65d4c83ab_submit" class="menu-link px-3 list-delete-form-button " data-submit-confirm-target="submitButton">Delete\n' +
	'        </button>\n' +
	'    </form>\n' +
	'\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                        </div>\n' +
	'                                        <!--end::Menu-->\n' +
	'                                    </td>\n' +
	'                                    <!--end::Action=-->\n' +
	'                                </tr>\n' +
	'                                                                                                                                                            <tr>\n' +
	'                                    <!--begin::Checkbox-->\n' +
	'                                    <td>\n' +
	'                                        <div class="form-check form-check-sm form-check-custom form-check-solid">\n' +
	'                                            <input name="bulkCheck" class="form-check-input" type="checkbox" value="f6c44563-a1b5-4cc3-9688-cd425d7b36fe" data-list-target="bulkCheck" data-action="list#onSingleListCheckBoxChange">\n' +
	'                                        </div>\n' +
	'                                    </td>\n' +
	'                                    <!--end::Checkbox-->\n' +
	'\n' +
	'                                                                            <td>\n' +
	'                                            <a href="/admin/workflow/f6c44563-a1b5-4cc3-9688-cd425d7b36fe/show" class="text-gray-800 text-hover-primary mb-1">\n' +
	'                                                TIENE STEPS\n' +
	'                                            </a>\n' +
	'                                        </td>\n' +
	'                                    \n' +
	'                                    <!--begin::Action=-->\n' +
	'                                    <td class="text-end">\n' +
	'                                        <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions\n' +
	'                                            <span class="svg-icon svg-icon-5 m-0">\n' +
	'                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->\n' +
	'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">\n' +
	'    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>\n' +
	'    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>\n' +
	'</svg>\n' +
	'<!--end::Svg Icon-->\n' +
	'\t\t\t\t\t\t\t\t\t</span>\n' +
	'                                        </a>\n' +
	'                                        <!--begin::Menu-->\n' +
	'                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/f6c44563-a1b5-4cc3-9688-cd425d7b36fe/show" class="menu-link px-3">View</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/f6c44563-a1b5-4cc3-9688-cd425d7b36fe/edit" class="menu-link px-3">Edit</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                \n' +
	'    <form action="http://127.0.0.1:8004/admin/workflow/delete" method="post" data-controller="submit-confirm" data-action="submit-confirm#onSubmit">\n' +
	'        <input type="hidden" name="token" value="f7c98d949a204486b807cbfdf996.2xlX08c6wwAL9gpLVES8PGL9ziJ_m3X6QpGgmfuk34c.uFQYsPFUgVRan2IMHQ3bfVa4rxsp-juOC_eUyojtstOZWBukrHarZnO5Pg">\n' +
	'        <input type="hidden" name="id" value="f6c44563-a1b5-4cc3-9688-cd425d7b36fe">\n' +
	'\n' +
	'        <button type="submit" id="form_f6c44563-a1b5-4cc3-9688-cd425d7b36fe_submit" class="menu-link px-3 list-delete-form-button " data-submit-confirm-target="submitButton">Delete\n' +
	'        </button>\n' +
	'    </form>\n' +
	'\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                        </div>\n' +
	'                                        <!--end::Menu-->\n' +
	'                                    </td>\n' +
	'                                    <!--end::Action=-->\n' +
	'                                </tr>\n' +
	'                                                                                                                                                            <tr>\n' +
	'                                    <!--begin::Checkbox-->\n' +
	'                                    <td>\n' +
	'                                        <div class="form-check form-check-sm form-check-custom form-check-solid">\n' +
	'                                            <input name="bulkCheck" class="form-check-input" type="checkbox" value="45eeb1fb-5ae9-47e5-8162-ca8b1a035061" data-list-target="bulkCheck" data-action="list#onSingleListCheckBoxChange">\n' +
	'                                        </div>\n' +
	'                                    </td>\n' +
	'                                    <!--end::Checkbox-->\n' +
	'\n' +
	'                                                                            <td>\n' +
	'                                            <a href="/admin/workflow/45eeb1fb-5ae9-47e5-8162-ca8b1a035061/show" class="text-gray-800 text-hover-primary mb-1">\n' +
	'                                                nico 2\n' +
	'                                            </a>\n' +
	'                                        </td>\n' +
	'                                    \n' +
	'                                    <!--begin::Action=-->\n' +
	'                                    <td class="text-end">\n' +
	'                                        <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions\n' +
	'                                            <span class="svg-icon svg-icon-5 m-0">\n' +
	'                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->\n' +
	'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">\n' +
	'    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>\n' +
	'    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>\n' +
	'</svg>\n' +
	'<!--end::Svg Icon-->\n' +
	'\t\t\t\t\t\t\t\t\t</span>\n' +
	'                                        </a>\n' +
	'                                        <!--begin::Menu-->\n' +
	'                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/45eeb1fb-5ae9-47e5-8162-ca8b1a035061/show" class="menu-link px-3">View</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/45eeb1fb-5ae9-47e5-8162-ca8b1a035061/edit" class="menu-link px-3">Edit</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                \n' +
	'    <form action="http://127.0.0.1:8004/admin/workflow/delete" method="post" data-controller="submit-confirm" data-action="submit-confirm#onSubmit">\n' +
	'        <input type="hidden" name="token" value="60945.hEjDoMr0K9n3JOIEJzF4gw_BL4uet7KIMp49pPXuSUw.5wWMw_yaaY2mTYpDbngfwjuETrLI1vz8e_gJ94anJBjGCY_XobhDv49r1g">\n' +
	'        <input type="hidden" name="id" value="45eeb1fb-5ae9-47e5-8162-ca8b1a035061">\n' +
	'\n' +
	'        <button type="submit" id="form_45eeb1fb-5ae9-47e5-8162-ca8b1a035061_submit" class="menu-link px-3 list-delete-form-button " data-submit-confirm-target="submitButton">Delete\n' +
	'        </button>\n' +
	'    </form>\n' +
	'\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                        </div>\n' +
	'                                        <!--end::Menu-->\n' +
	'                                    </td>\n' +
	'                                    <!--end::Action=-->\n' +
	'                                </tr>\n' +
	'                                                                                                                                                            <tr>\n' +
	'                                    <!--begin::Checkbox-->\n' +
	'                                    <td>\n' +
	'                                        <div class="form-check form-check-sm form-check-custom form-check-solid">\n' +
	'                                            <input name="bulkCheck" class="form-check-input" type="checkbox" value="cb6b0689-9e8a-40f4-9017-aea84a23accd" data-list-target="bulkCheck" data-action="list#onSingleListCheckBoxChange">\n' +
	'                                        </div>\n' +
	'                                    </td>\n' +
	'                                    <!--end::Checkbox-->\n' +
	'\n' +
	'                                                                            <td>\n' +
	'                                            <a href="/admin/workflow/cb6b0689-9e8a-40f4-9017-aea84a23accd/show" class="text-gray-800 text-hover-primary mb-1">\n' +
	'                                                test\n' +
	'                                            </a>\n' +
	'                                        </td>\n' +
	'                                    \n' +
	'                                    <!--begin::Action=-->\n' +
	'                                    <td class="text-end">\n' +
	'                                        <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions\n' +
	'                                            <span class="svg-icon svg-icon-5 m-0">\n' +
	'                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->\n' +
	'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">\n' +
	'    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>\n' +
	'    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>\n' +
	'</svg>\n' +
	'<!--end::Svg Icon-->\n' +
	'\t\t\t\t\t\t\t\t\t</span>\n' +
	'                                        </a>\n' +
	'                                        <!--begin::Menu-->\n' +
	'                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/cb6b0689-9e8a-40f4-9017-aea84a23accd/show" class="menu-link px-3">View</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/cb6b0689-9e8a-40f4-9017-aea84a23accd/edit" class="menu-link px-3">Edit</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                \n' +
	'    <form action="http://127.0.0.1:8004/admin/workflow/delete" method="post" data-controller="submit-confirm" data-action="submit-confirm#onSubmit">\n' +
	'        <input type="hidden" name="token" value="bdde22101ad1ea5519dab.tFFbIc8z5iMV--LIKcMba3yNTuxibIL9ctwPgwzrCR0.1xwUQvldpHdEkoqPYIp8KkjIL9U0DcyJO7o70H-iZEn2EBdWpH-ORW201g">\n' +
	'        <input type="hidden" name="id" value="cb6b0689-9e8a-40f4-9017-aea84a23accd">\n' +
	'\n' +
	'        <button type="submit" id="form_cb6b0689-9e8a-40f4-9017-aea84a23accd_submit" class="menu-link px-3 list-delete-form-button " data-submit-confirm-target="submitButton">Delete\n' +
	'        </button>\n' +
	'    </form>\n' +
	'\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                        </div>\n' +
	'                                        <!--end::Menu-->\n' +
	'                                    </td>\n' +
	'                                    <!--end::Action=-->\n' +
	'                                </tr>\n' +
	'                                                                                                                                                            <tr>\n' +
	'                                    <!--begin::Checkbox-->\n' +
	'                                    <td>\n' +
	'                                        <div class="form-check form-check-sm form-check-custom form-check-solid">\n' +
	'                                            <input name="bulkCheck" class="form-check-input" type="checkbox" value="02da4750-9409-4560-b504-b80f842ff6ab" data-list-target="bulkCheck" data-action="list#onSingleListCheckBoxChange">\n' +
	'                                        </div>\n' +
	'                                    </td>\n' +
	'                                    <!--end::Checkbox-->\n' +
	'\n' +
	'                                                                            <td>\n' +
	'                                            <a href="/admin/workflow/02da4750-9409-4560-b504-b80f842ff6ab/show" class="text-gray-800 text-hover-primary mb-1">\n' +
	'                                                new name\n' +
	'                                            </a>\n' +
	'                                        </td>\n' +
	'                                    \n' +
	'                                    <!--begin::Action=-->\n' +
	'                                    <td class="text-end">\n' +
	'                                        <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions\n' +
	'                                            <span class="svg-icon svg-icon-5 m-0">\n' +
	'                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->\n' +
	'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">\n' +
	'    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>\n' +
	'    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>\n' +
	'</svg>\n' +
	'<!--end::Svg Icon-->\n' +
	'\t\t\t\t\t\t\t\t\t</span>\n' +
	'                                        </a>\n' +
	'                                        <!--begin::Menu-->\n' +
	'                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/02da4750-9409-4560-b504-b80f842ff6ab/show" class="menu-link px-3">View</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/02da4750-9409-4560-b504-b80f842ff6ab/edit" class="menu-link px-3">Edit</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                \n' +
	'    <form action="http://127.0.0.1:8004/admin/workflow/delete" method="post" data-controller="submit-confirm" data-action="submit-confirm#onSubmit">\n' +
	'        <input type="hidden" name="token" value="33f52ff46cca594dd733ac2.FneZAidzg5YCkKT4pCNgpm7EmEDr8pp4f_um02FRIlg.dTrWYREdwcJT-cy_7WoH51qB-Xm9k9QMNp2SgBIYTwxUNtV1TD_r8HrfkA">\n' +
	'        <input type="hidden" name="id" value="02da4750-9409-4560-b504-b80f842ff6ab">\n' +
	'\n' +
	'        <button type="submit" id="form_02da4750-9409-4560-b504-b80f842ff6ab_submit" class="menu-link px-3 list-delete-form-button " data-submit-confirm-target="submitButton">Delete\n' +
	'        </button>\n' +
	'    </form>\n' +
	'\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                        </div>\n' +
	'                                        <!--end::Menu-->\n' +
	'                                    </td>\n' +
	'                                    <!--end::Action=-->\n' +
	'                                </tr>\n' +
	'                                                                                                                                                            <tr>\n' +
	'                                    <!--begin::Checkbox-->\n' +
	'                                    <td>\n' +
	'                                        <div class="form-check form-check-sm form-check-custom form-check-solid">\n' +
	'                                            <input name="bulkCheck" class="form-check-input" type="checkbox" value="cfc1331f-ce26-45f9-b1b8-cb775a563967" data-list-target="bulkCheck" data-action="list#onSingleListCheckBoxChange">\n' +
	'                                        </div>\n' +
	'                                    </td>\n' +
	'                                    <!--end::Checkbox-->\n' +
	'\n' +
	'                                                                            <td>\n' +
	'                                            <a href="/admin/workflow/cfc1331f-ce26-45f9-b1b8-cb775a563967/show" class="text-gray-800 text-hover-primary mb-1">\n' +
	'                                                asd\n' +
	'                                            </a>\n' +
	'                                        </td>\n' +
	'                                    \n' +
	'                                    <!--begin::Action=-->\n' +
	'                                    <td class="text-end">\n' +
	'                                        <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions\n' +
	'                                            <span class="svg-icon svg-icon-5 m-0">\n' +
	'                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->\n' +
	'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">\n' +
	'    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>\n' +
	'    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>\n' +
	'</svg>\n' +
	'<!--end::Svg Icon-->\n' +
	'\t\t\t\t\t\t\t\t\t</span>\n' +
	'                                        </a>\n' +
	'                                        <!--begin::Menu-->\n' +
	'                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/cfc1331f-ce26-45f9-b1b8-cb775a563967/show" class="menu-link px-3">View</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/cfc1331f-ce26-45f9-b1b8-cb775a563967/edit" class="menu-link px-3">Edit</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                \n' +
	'    <form action="http://127.0.0.1:8004/admin/workflow/delete" method="post" data-controller="submit-confirm" data-action="submit-confirm#onSubmit">\n' +
	'        <input type="hidden" name="token" value="7.IKfsP5OuIV2pxM2QJzoa0LX53IWPnUZQk1_vroWF3f8.Q-qjXKXAYwn4raXXbnN9kYG8vbzZ_Agk2jnb_fbMsKti5qBI-OJJO9GL-Q">\n' +
	'        <input type="hidden" name="id" value="cfc1331f-ce26-45f9-b1b8-cb775a563967">\n' +
	'\n' +
	'        <button type="submit" id="form_cfc1331f-ce26-45f9-b1b8-cb775a563967_submit" class="menu-link px-3 list-delete-form-button " data-submit-confirm-target="submitButton">Delete\n' +
	'        </button>\n' +
	'    </form>\n' +
	'\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                        </div>\n' +
	'                                        <!--end::Menu-->\n' +
	'                                    </td>\n' +
	'                                    <!--end::Action=-->\n' +
	'                                </tr>\n' +
	'                                                                                                                                                            <tr>\n' +
	'                                    <!--begin::Checkbox-->\n' +
	'                                    <td>\n' +
	'                                        <div class="form-check form-check-sm form-check-custom form-check-solid">\n' +
	'                                            <input name="bulkCheck" class="form-check-input" type="checkbox" value="c23d4b22-7bf9-48e2-9df3-06c3067d4c8c" data-list-target="bulkCheck" data-action="list#onSingleListCheckBoxChange">\n' +
	'                                        </div>\n' +
	'                                    </td>\n' +
	'                                    <!--end::Checkbox-->\n' +
	'\n' +
	'                                                                            <td>\n' +
	'                                            <a href="/admin/workflow/c23d4b22-7bf9-48e2-9df3-06c3067d4c8c/show" class="text-gray-800 text-hover-primary mb-1">\n' +
	'                                                nico 2\n' +
	'                                            </a>\n' +
	'                                        </td>\n' +
	'                                    \n' +
	'                                    <!--begin::Action=-->\n' +
	'                                    <td class="text-end">\n' +
	'                                        <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions\n' +
	'                                            <span class="svg-icon svg-icon-5 m-0">\n' +
	'                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->\n' +
	'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">\n' +
	'    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>\n' +
	'    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>\n' +
	'</svg>\n' +
	'<!--end::Svg Icon-->\n' +
	'\t\t\t\t\t\t\t\t\t</span>\n' +
	'                                        </a>\n' +
	'                                        <!--begin::Menu-->\n' +
	'                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/c23d4b22-7bf9-48e2-9df3-06c3067d4c8c/show" class="menu-link px-3">View</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/c23d4b22-7bf9-48e2-9df3-06c3067d4c8c/edit" class="menu-link px-3">Edit</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                \n' +
	'    <form action="http://127.0.0.1:8004/admin/workflow/delete" method="post" data-controller="submit-confirm" data-action="submit-confirm#onSubmit">\n' +
	'        <input type="hidden" name="token" value="b723ccfb7e0523926602ed.lStgGqoWLSfrI_LJ4wMWEOiiZcmbf5tBIHY-fcTFfig.9mYveZx4b3O6SpqOqkpxUdznBPDNHtU1aRAKLreME3zXaixtwVpFQZNsxg">\n' +
	'        <input type="hidden" name="id" value="c23d4b22-7bf9-48e2-9df3-06c3067d4c8c">\n' +
	'\n' +
	'        <button type="submit" id="form_c23d4b22-7bf9-48e2-9df3-06c3067d4c8c_submit" class="menu-link px-3 list-delete-form-button " data-submit-confirm-target="submitButton">Delete\n' +
	'        </button>\n' +
	'    </form>\n' +
	'\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                        </div>\n' +
	'                                        <!--end::Menu-->\n' +
	'                                    </td>\n' +
	'                                    <!--end::Action=-->\n' +
	'                                </tr>\n' +
	'                                                                                                                                                            <tr>\n' +
	'                                    <!--begin::Checkbox-->\n' +
	'                                    <td>\n' +
	'                                        <div class="form-check form-check-sm form-check-custom form-check-solid">\n' +
	'                                            <input name="bulkCheck" class="form-check-input" type="checkbox" value="a2e3bb95-63c1-4b81-b9ee-6c24d551a4e9" data-list-target="bulkCheck" data-action="list#onSingleListCheckBoxChange">\n' +
	'                                        </div>\n' +
	'                                    </td>\n' +
	'                                    <!--end::Checkbox-->\n' +
	'\n' +
	'                                                                            <td>\n' +
	'                                            <a href="/admin/workflow/a2e3bb95-63c1-4b81-b9ee-6c24d551a4e9/show" class="text-gray-800 text-hover-primary mb-1">\n' +
	'                                                nico 2\n' +
	'                                            </a>\n' +
	'                                        </td>\n' +
	'                                    \n' +
	'                                    <!--begin::Action=-->\n' +
	'                                    <td class="text-end">\n' +
	'                                        <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions\n' +
	'                                            <span class="svg-icon svg-icon-5 m-0">\n' +
	'                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->\n' +
	'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">\n' +
	'    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>\n' +
	'    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>\n' +
	'</svg>\n' +
	'<!--end::Svg Icon-->\n' +
	'\t\t\t\t\t\t\t\t\t</span>\n' +
	'                                        </a>\n' +
	'                                        <!--begin::Menu-->\n' +
	'                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/a2e3bb95-63c1-4b81-b9ee-6c24d551a4e9/show" class="menu-link px-3">View</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/a2e3bb95-63c1-4b81-b9ee-6c24d551a4e9/edit" class="menu-link px-3">Edit</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                \n' +
	'    <form action="http://127.0.0.1:8004/admin/workflow/delete" method="post" data-controller="submit-confirm" data-action="submit-confirm#onSubmit">\n' +
	'        <input type="hidden" name="token" value="adccb25da073efacbdd12ef3.9w5Nshw-WKwSmYal-yFhtuNQP1P745fzSHVKYbpnVpQ.lEMC0SpQGvhD8O7ismgG99cVXmqtgtmHARN-MskuO8C1TwHFd3IwymrWsg">\n' +
	'        <input type="hidden" name="id" value="a2e3bb95-63c1-4b81-b9ee-6c24d551a4e9">\n' +
	'\n' +
	'        <button type="submit" id="form_a2e3bb95-63c1-4b81-b9ee-6c24d551a4e9_submit" class="menu-link px-3 list-delete-form-button " data-submit-confirm-target="submitButton">Delete\n' +
	'        </button>\n' +
	'    </form>\n' +
	'\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                        </div>\n' +
	'                                        <!--end::Menu-->\n' +
	'                                    </td>\n' +
	'                                    <!--end::Action=-->\n' +
	'                                </tr>\n' +
	'                                                                                                                                                            <tr>\n' +
	'                                    <!--begin::Checkbox-->\n' +
	'                                    <td>\n' +
	'                                        <div class="form-check form-check-sm form-check-custom form-check-solid">\n' +
	'                                            <input name="bulkCheck" class="form-check-input" type="checkbox" value="string" data-list-target="bulkCheck" data-action="list#onSingleListCheckBoxChange">\n' +
	'                                        </div>\n' +
	'                                    </td>\n' +
	'                                    <!--end::Checkbox-->\n' +
	'\n' +
	'                                                                            <td>\n' +
	'                                            <a href="/admin/workflow/string/show" class="text-gray-800 text-hover-primary mb-1">\n' +
	'                                                string\n' +
	'                                            </a>\n' +
	'                                        </td>\n' +
	'                                    \n' +
	'                                    <!--begin::Action=-->\n' +
	'                                    <td class="text-end">\n' +
	'                                        <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions\n' +
	'                                            <span class="svg-icon svg-icon-5 m-0">\n' +
	'                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->\n' +
	'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">\n' +
	'    <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black"></rect>\n' +
	'    <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black"></rect>\n' +
	'</svg>\n' +
	'<!--end::Svg Icon-->\n' +
	'\t\t\t\t\t\t\t\t\t</span>\n' +
	'                                        </a>\n' +
	'                                        <!--begin::Menu-->\n' +
	'                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/string/show" class="menu-link px-3">View</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                <a href="/admin/workflow/string/edit" class="menu-link px-3">Edit</a>\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                            <!--begin::Menu item-->\n' +
	'                                            <div class="menu-item px-3">\n' +
	'                                                \n' +
	'    <form action="http://127.0.0.1:8004/admin/workflow/delete" method="post" data-controller="submit-confirm" data-action="submit-confirm#onSubmit">\n' +
	'        <input type="hidden" name="token" value="0.wFIItkNjNzKm-RVrhuSH8GbsLrffH_JUPksG-9R2Qis.ox9H1XUNdWb3kH0sz63gsVKpT46Jfrwgdy0yqKc_L3-CE0TBKC9fVN62IQ">\n' +
	'        <input type="hidden" name="id" value="string">\n' +
	'\n' +
	'        <button type="submit" id="form_string_submit" class="menu-link px-3 list-delete-form-button " data-submit-confirm-target="submitButton">Delete\n' +
	'        </button>\n' +
	'    </form>\n' +
	'\n' +
	'                                            </div>\n' +
	'                                            <!--end::Menu item-->\n' +
	'                                        </div>\n' +
	'                                        <!--end::Menu-->\n' +
	'                                    </td>\n' +
	'                                    <!--end::Action=-->\n' +
	'                                </tr>\n' +
	'                                                                            </tbody>\n' +
	'                        <!--end::Table body-->\n' +
	'                    </table>\n' +
	'                            </div>\n' +
	'            <!--end::Table-->\n' +
	'\n' +
	'            <div class="row pt-10">\n' +
	'\n' +
	'                <div class="col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start"></div>\n' +
	'                <div class="col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end">\n' +
	'                    <div class="dataTables_paginate paging_simple_numbers">\n' +
	'                            <nav>\n' +
	'                        <ul class="pagination">\n' +
	'\n' +
	'                            <li class="page-item previous disabled">\n' +
	'                    <span class="page-link"><i class="previous"></i></span>\n' +
	'                </li>\n' +
	'            \n' +
	'            \n' +
	'                                                <li class="page-item active">\n' +
	'                        <span class="page-link">1</span>\n' +
	'                    </li>\n' +
	'                \n' +
	'                                                <li class="page-item">\n' +
	'                        <a class="page-link" href="/admin/workflow/list?page=2">2</a>\n' +
	'                    </li>\n' +
	'                \n' +
	'            \n' +
	'            \n' +
	'                            <li class="page-item next">\n' +
	'                    <a class="page-link" rel="next" href="/admin/workflow/list?page=2"><i class="next"></i></a>\n' +
	'                </li>\n' +
	'                    </ul>\n' +
	'    </nav>\n' +
	'\n' +
	'                    </div>\n' +
	'                </div>\n' +
	'            </div>\n' +
	'        </div>\n' +
	'        <!--end::List body-->\n' +
	'    </div>'
