<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyInfo\Extractor\PhpStanExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfonycasts\MicroMapper\MicroMapperInterface;

class EntityMappingService
{
    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
        private readonly MicroMapperInterface $mapper
    ) {
    }

    public function getClassMetadata(string $entityFqcn)
    {
        return $this->getManager($entityFqcn)->getClassMetadata($entityFqcn);
    }

    public function getFieldType(string $entityFqcn, string $fieldName): ?string
    {
        $classMetaData = $this->getClassMetadata($entityFqcn);
        if (in_array($fieldName, $classMetaData->getAssociationNames())) {
            return 'association';
        }

        return $classMetaData->getTypeOfField($fieldName);
    }

    /**
     * Get entity properties.
     */
    public function getEntityPropertiesNames(string $entityFqcn): array
    {
        $classMetaData = $this->getClassMetadata($entityFqcn);

        return [...$classMetaData->getFieldNames(), ...$classMetaData->getAssociationNames()];
    }

    /**
     * Get entity properties.
     */
    public function getEntityProperties(string $entityFqcn): array
    {
        $classMetaData = $this->getClassMetadata($entityFqcn);

        return [...$classMetaData->fieldMappings, ...$classMetaData->associationMappings];
    }

    public function getFieldNames($entity): array
    {
        $manager = $this->getManager(\get_class($entity));
        $classMetaData = $manager->getClassMetadata($entity);

        return $classMetaData->getFieldNames();
    }

    /**
     * @throws \ReflectionException
     */
    public static function getClassName($entity): string
    {
        if (is_string($entity)) {
            return str_starts_with($entity, "\\") ? $entity : '\\'.$entity;
        }
        $rfc = new \ReflectionClass($entity);

        return str_starts_with($rfc->getName(), "\\") ? $rfc->getName() : '\\'.$rfc->getName();
    }

    public static function getClassShortName($class): string
    {
        $class = is_object($class) ? get_class($class) : $class;
        return substr(strrchr($class, '\\'), 1);
    }

    private function getManager($entity): ObjectManager
    {
        return $this->managerRegistry->getManagerForClass($entity);
    }

    public function fromObjectToEntityMapper(object $fromObject, object $toEntity, ?int $maxDepth = 1): object
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $propertyInfoExtractor = new PropertyInfoExtractor(
            [new ReflectionExtractor()],
            [new ReflectionExtractor()]
        );

        $fromObjectClassName = get_class($fromObject);
        $fromObjectProperties = $propertyInfoExtractor->getProperties($fromObjectClassName);
        $toEntityClassName = get_class($toEntity);
        $toEntityProperties = $this->getEntityProperties($toEntityClassName);
        // loop into the dto properties in order to populate the entity
        foreach ($fromObjectProperties as $property) {
            // If the dto property value is null skip #TODO review this, because I should be able to send null values
            if (null === $propertyAccessor->getValue($fromObject, $property)) {
                continue;
            }

            // check if the current dto property does not exist in the entity
            if (!in_array($property, array_keys($toEntityProperties))) {
                continue;
            }

            // the entity property exist so now analyze the type of the property
            switch ($toEntityProperties[$property]['type']) {
                // Relation types
                case ClassMetadataInfo::MANY_TO_ONE:

                    // Now we have to map from DTO PROPERTY to ENTITY PROPERTY related to that property
                    // i.e: AddressDto to User's Address ( Address * -> 1 User )

                    // DTO PROPERTY part
                    // $fromDtoPropertyValue is an object i.e: AddressDto
                    // $fromEntity = User Dto
                    // $property = address property name of the User DTO instance
                    $fromDtoPropertyValue = $propertyAccessor->getValue($fromObject, $property);

                    // PROPERTY DTO part
                    // $toDtoClassName = User DTO class name
                    // $property['fieldName'] = address property name of the User instance
                    // $toDtoPropClass is a string i.e: App\Resources\\AddressDto
                    $toEntityPropClass = $toEntityProperties[$property]['targetEntity'];
                    // $e = explode('\\',$toDtoPropClass);
                    // $toDtoPropClassResourceClassFqcn = 'App\\Resources\\'.end($e);

                    // Call the mapper from Address instance to AddressDto
                    $value = $this->mapper->map($fromDtoPropertyValue, $toEntityPropClass, [MicroMapperInterface::MAX_DEPTH => $maxDepth]);
                    $propertyAccessor->setValue($toEntity, $property, $value);
                    break;
                case ClassMetadataInfo::TO_ONE:
                    throw new \Exception('TO_ONE not implemented');
                case ClassMetadataInfo::ONE_TO_MANY:
                    // We have to map from DTO PROPERTY to ENTITY PROPERTY related to that property
                    // i.e: Category's Posts to collection of PostDto ( Category 1 -> * Posts )

                    // DTO PROPERTY part
                    // $fromDtoPropertyValue is a collection i.e: the Posts of a Category
                    // $fromObject = Category Dto
                    // $property = posts property name of the Category instance
                    $fromDtoPropertyValue = $propertyAccessor->getValue($fromObject, $property);


                    if (0 === count($fromDtoPropertyValue)) {
                        break;
                    }

                    // PROPERTY DTO part
                    // $toDtoClassName = User DTO class name
                    // $property['fieldName'] = address property name of the User instance
                    // $toDtoPropClass is a string i.e: App\Resources\\AddressDto
                    $toEntityPropClass = $toEntityProperties[$property]['targetEntity'];


                    // ENTITY PROPERTY part
                    // $toDtoClassName = User DTO class name
                    // $property['fieldName'] = address property name of the User instance
                    // $toDtoPropClass is a string i.e: App\Resources\\AddressDto

                    $mappedValues = new ArrayCollection();
                    foreach ($fromDtoPropertyValue as $propertyValue ) {
                        //If the entity ID is empty, we are trying to map an entity that was mapped before in the second level
                        //so its properties are all empty, we cannot continue mapping here
                        if(empty($propertyValue->getId())) {
                            continue;
                        }
                        // Call the mapper from Post instance to PostDto
                        $mappedValues->add($this->mapper->map($propertyValue, $toEntityPropClass, [MicroMapperInterface::MAX_DEPTH => $maxDepth]));
                    }
                    if (count($mappedValues)) {
                        $propertyAccessor->setValue($toEntity, $property, $mappedValues);
                    }

                    break;

                    throw new \Exception('ONE_TO_MANY not implemented');
                case ClassMetadataInfo::MANY_TO_MANY:
                     break;
                    throw new \Exception('MANY_TO_MANY not implemented');
                case ClassMetadataInfo::TO_MANY:
                    throw new \Exception('TO_MANY not implemented');
                case ClassMetadataInfo::ONE_TO_ONE:
                    throw new \Exception('ONE_TO_ONE not implemented');
                default:
                    $propertyAccessor->setValue($toEntity, $property, $propertyAccessor->getValue($fromObject, $property));
            }
        }

        return $toEntity;
    }

    public function fromEntityToDtoMapper(object $fromEntity, object $toDto, ?int $maxDepth = 1): object
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $propertyInfoExtractor = new PropertyInfoExtractor(
            [new ReflectionExtractor()],
            [new ReflectionExtractor(), new PhpStanExtractor()]
        );
        $fromEntityClassName = get_class($fromEntity);
        $fromEntityProperties = $this->getEntityProperties($fromEntityClassName);
        $toDtoClassName = get_class($toDto);
        $toDtoProperties = $propertyInfoExtractor->getProperties($toDtoClassName);
        // loop into the entity properties in order to populate the dto
        foreach ($fromEntityProperties as $property) {
            // check if the current entity property does not exist in the dto
            if (!in_array($property['fieldName'], $toDtoProperties)) {
                continue;
            }

            // the entity property exist so now analyze the type of the property
            switch ($property['type']) {
                // Relation types
                case ClassMetadataInfo::ONE_TO_ONE:
                case ClassMetadataInfo::MANY_TO_ONE:
                    // Now we have to map from ENTITY PROPERTY to ENTITY PROPERTY DTO related to that property
                    // i.e: User's Address to AddressDto

                    // ENTITY PROPERTY part
                    // $fromEntityPropertyValue is an object i.e: the Address of a User
                    // $fromEntity = User instance
                    // $property['fieldName'] = address property name of the User instance
                    $fromEntityPropertyValue = $propertyAccessor->getValue($fromEntity, $property['fieldName']);
                    if (null === $fromEntityPropertyValue) {
                        break;
                    }
                    // PROPERTY DTO part
                    // $toDtoClassName = User DTO class name
                    // $property['fieldName'] = address property name of the User instance
                    // $toDtoPropClass is a string i.e: App\Resources\\AddressDto
                    $toDtoPropertyTypeInformation = $propertyInfoExtractor->getTypes($toDtoClassName, $property['fieldName']);
                    $toDtoPropClass = $toDtoPropertyTypeInformation[0]->getClassName();
                    // $e = explode('\\',$toDtoPropClass);
                    // $toDtoPropClassResourceClassFqcn = 'App\\Resources\\'.end($e);

                    // Call the mapper from Address instance to AddressDto
                    $value = $this->mapper->map($fromEntityPropertyValue, $toDtoPropClass, [MicroMapperInterface::MAX_DEPTH => $maxDepth]);
                    $propertyAccessor->setValue($toDto, $property['fieldName'], $value);
                    break;
                case ClassMetadataInfo::ONE_TO_MANY:
                    // We have to map from ENTITY PROPERTY to ENTITY PROPERTY DTO related to that property
                    // i.e: Category's Posts to collection of PostDto

                    // ENTITY PROPERTY part
                    // $fromEntityPropertyValue is a collection i.e: the Posts of a Category
                    // $fromEntity = Category instance
                    // $property['fieldName'] = posts property name of the Category instance
                    $fromEntityPropertyValue = $propertyAccessor->getValue($fromEntity, $property['fieldName']);

                    if (0 === count($fromEntityPropertyValue)) {
                        break;
                    }


                    // PROPERTY DTO part
                    // $toDtoClassName = User DTO class name
                    // $property['fieldName'] = address property name of the User instance
                    // $toDtoPropClass is a string i.e: App\Resources\\AddressDto
                    $phpStanExtractor = new PhpStanExtractor();
                    $toDtoPropertyTypeInformation = $phpStanExtractor->getTypes($toDtoClassName, $property['fieldName']);
                    if(!$toDtoPropertyTypeInformation) {
                        throw new \Exception(
                            sprintf('No annotation found for property: [%s] on class [%s].
                            You must provide a property annotation for non typed properties, i.e: @var iterable<LocationDto>',
                                $property['fieldName'],
                                $toDtoClassName
                            )
                        );
                    }
                    $toDtoPropClass = $toDtoPropertyTypeInformation[0]->getCollectionValueTypes()[0]->getClassName();

                    $mappedValues = new ArrayCollection();
                    foreach ($fromEntityPropertyValue as $propertyValue ) {
                        // Call the mapper from Post instance to PostDto
                        $mappedValues->add($this->mapper->map($propertyValue, $toDtoPropClass, [MicroMapperInterface::MAX_DEPTH => 1]));
                    }
                    $propertyAccessor->setValue($toDto, $property['fieldName'], $mappedValues);

                    break;
                case ClassMetadataInfo::MANY_TO_MANY:
                    // Now we have to map from ENTITY PROPERTY to ENTITY PROPERTY DTO related to that property
                    // i.e: Question's Tags to TagsDto

                    // ENTITY PROPERTY part
                    // $fromEntityPropertyValue is a collection i.e: the tags of a Question
                    // $fromEntity = Question instance
                    // $property['fieldName'] = tags property name of the Question instance
                    $fromEntityPropertyValue = $propertyAccessor->getValue($fromEntity, $property['fieldName']);

                    if (0 === count($fromEntityPropertyValue)) {
                        break;
                    }


                    // PROPERTY DTO part
                    // $toDtoClassName = Tag DTO class name
                    // $property['fieldName'] = tags property name of the Question instance
                    // $toDtoPropClass is a string i.e: App\Resources\\TagDto
                    $phpStanExtractor = new PhpStanExtractor();
                    $toDtoPropertyTypeInformation = $phpStanExtractor->getTypes($toDtoClassName, $property['fieldName']);
                    if(!$toDtoPropertyTypeInformation) {
                        throw new \Exception(
                            sprintf('No annotation found for property: [%s] on class [%s].
                            You must provide a property annotation for non typed properties, i.e: @var iterable<LocationDto>',
                                $property['fieldName'],
                                $toDtoClassName
                            )
                        );
                    }
                    $toDtoPropClass = $toDtoPropertyTypeInformation[0]->getCollectionValueTypes()[0]->getClassName();

                    $mappedValues = new ArrayCollection();
                    foreach ($fromEntityPropertyValue as $propertyValue ) {
                        // Call the mapper from Tag instance to TagDto
                        $mappedValues->add($this->mapper->map($propertyValue, $toDtoPropClass, [MicroMapperInterface::MAX_DEPTH => 0]));
                    }
                    $propertyAccessor->setValue($toDto, $property['fieldName'], $mappedValues);

                case ClassMetadataInfo::TO_MANY:
                    break;
                case ClassMetadataInfo::ONE_TO_ONE:
                    throw new \Exception('ONE_TO_ONE not implemented');
                case ClassMetadataInfo::TO_ONE:
                    throw new \Exception('TO_ONE not implemented');
                    default:
                    try {
                        // Is possible that the entity has the property not initialized when running this line
                        // $propertyAccessor->getValue($fromEntity, $property['fieldName'])
                        // so it will throw an error trying to read.
                        // then we do not need to map this property
                        $propertyAccessor->setValue($toDto, $property['fieldName'], $propertyAccessor->getValue($fromEntity, $property['fieldName']));
                    } catch (\Exception $e) {
                    }
            }
        }

        return $toDto;
    }

}
