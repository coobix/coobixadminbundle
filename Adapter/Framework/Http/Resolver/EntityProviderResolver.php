<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\Resolver;

use Coobix\AdminBundle\Adapter\Framework\Http\State\Provider\CrudEntity\CrudEntityProviderInterface;

class EntityProviderResolver
{
    public function __construct(private readonly CrudEntityProviderInterface $provider)
    {
    }

    public function resolveProvider(string $classFqcn, array $context = []): ?object
    {
        return $this->provider->provide($classFqcn, $context);
    }
}
