<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\Resolver;

use Coobix\AdminBundle\Adapter\Framework\Http\State\Processor\CrudEntity\CrudEntityProcessorInterface;

class EntityProcessorResolver
{
    public function __construct(private readonly CrudEntityProcessorInterface $processor)
    {
    }

    public function resolveProcessor(object $crudEntity, array $context = []): ?object
    {
        return $this->processor->process($crudEntity, $context);
    }
}
