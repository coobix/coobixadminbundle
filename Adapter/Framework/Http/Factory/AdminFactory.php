<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\Factory;

use Coobix\AdminBundle\Domain\Exception\CrudNotFoundException;
use Coobix\AdminBundle\Domain\Model\Admin;

final class AdminFactory
{
    private iterable $cruds;

    public function __construct(iterable $cruds)
    {
        $this->cruds = $cruds instanceof \Traversable ? iterator_to_array($cruds) : $cruds;
    }

    public function create(string $crudRoute): Admin
    {
        if (empty($this->cruds[$crudRoute])) {
            // TODO aprender como hacer classes excepciones
            throw new CrudNotFoundException(' Crud class for route "'.$crudRoute.'" not found.');
        }

        return $this->cruds[$crudRoute];
    }
}
