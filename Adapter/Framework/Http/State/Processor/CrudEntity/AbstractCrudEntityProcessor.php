<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\State\Processor\CrudEntity;

abstract class AbstractCrudEntityProcessor implements CrudEntityProcessorInterface
{
    protected ?CrudEntityProcessorInterface $successor = null;

    public function processNext(object $crudEntity, array $context = []): ?object
    {
        return $this->successor?->process($crudEntity, $context);
    }

    public function setSuccessor(?CrudEntityProcessorInterface $successor): void
    {
        $this->successor = $successor;
    }

    public static function getDefaultPriority(): int
    {
        return 1;
    }
}
