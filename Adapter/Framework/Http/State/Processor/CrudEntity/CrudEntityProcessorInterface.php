<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\State\Processor\CrudEntity;

interface CrudEntityProcessorInterface
{
    public function process(object $crudEntity, array $context = []): ?object;

    public function setSuccessor(?CrudEntityProcessorInterface $successor): void;

    public function supports(object $crudEntity, array $context = []): bool;

    public static function getDefaultPriority(): int;
}
