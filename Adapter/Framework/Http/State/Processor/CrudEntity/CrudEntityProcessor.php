<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\State\Processor\CrudEntity;

use AutoMapper\AutoMapper;
use Coobix\AdminBundle\Adapter\Framework\Http\Service\EntityMappingService;
use Coobix\AdminBundle\Application\UseCase\CreateCrudEntity\CreateCrudEntity;
use Coobix\AdminBundle\Application\UseCase\GetCrudEntityById\Dto\GetCrudEntityByIdInputDto;
use Coobix\AdminBundle\Application\UseCase\GetCrudEntityById\GetCrudEntityById;
use Coobix\AdminBundle\Domain\Exception\ProcessorNotFoundException;
use Coobix\AdminBundle\Domain\Exception\ProcessorOperationNotFoundException;
use Coobix\AdminBundle\Domain\Model\Admin;
use Symfony\Component\Serializer\SerializerInterface;
use Symfonycasts\MicroMapper\MicroMapperInterface;

class CrudEntityProcessor extends AbstractCrudEntityProcessor
{
    public const SUPPORTED_ACTIONS = [
        Admin::ACTION_CREATE,
        Admin::ACTION_UPDATE,
    ];

    public function __construct(
        private readonly CreateCrudEntity $useCase,
        private readonly EntityMappingService $mappingService,
        private readonly SerializerInterface $serializer,
        //private readonly MicroMapperInterface $mapper,
        private readonly AutoMapper $mapper,
        private readonly GetCrudEntityById $getCrudEntityByIdUseCase,
    ) {
    }

    public function process($crudEntity, array $context = []): ?object
    {
        if (!$this->supports($crudEntity, $context)) {
            throw new ProcessorNotFoundException(sprintf('Processor for class: [%s] and operation: [%s] not found', $crudEntity, $context['operation']));
        }

        switch ($context['operation']) {
            case Admin::ACTION_CREATE:
                if ($this->mappingService->getClassName($context['entity']) !== $this->mappingService->getClassName($crudEntity)) {
                    $crudEntity = $this->mapper->map($crudEntity, $context['entity'], ['skip_null_values' => true]);
                }

                $this->useCase->handle($crudEntity);

                return $crudEntity;

            case Admin::ACTION_UPDATE:
                if ($this->mappingService->getClassName($context['entity']) !== $this->mappingService->getClassName($crudEntity)) {
                    $dbEntity = $this->getCrudEntityByIdUseCase->handle(GetCrudEntityByIdInputDto::create($context['entity'], $crudEntity->id));
                    $crudEntity = $this->mapper->map($crudEntity, $dbEntity, ['skip_null_values' => true]);
                    //$crudEntity = $this->mapper->map($crudEntity, $context['entity']);
                }
                $this->useCase->handle($crudEntity);

                return $crudEntity;
        }

        throw new ProcessorOperationNotFoundException(sprintf('Operation [%s] for processor: [%s] not found', $context['operation'], CrudEntityProcessor::class));
    }

    public function supports(object $crudEntity, array $context = []): bool
    {
        return in_array($context['operation'], self::SUPPORTED_ACTIONS);
    }

    public static function getDefaultPriority(): int
    {
        return -1;
    }
}
