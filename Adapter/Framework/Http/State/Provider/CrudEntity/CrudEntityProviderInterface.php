<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\State\Provider\CrudEntity;

interface CrudEntityProviderInterface
{
    public function provide(string $classFqcn, array $context = []);

    public function setSuccessor(?CrudEntityProviderInterface $successor): void;

    public function supports(string $classFqcn, array $context = []): bool;

    public static function getDefaultPriority(): int;
}
