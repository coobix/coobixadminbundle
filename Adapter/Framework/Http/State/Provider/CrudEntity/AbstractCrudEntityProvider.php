<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\State\Provider\CrudEntity;

abstract class AbstractCrudEntityProvider implements CrudEntityProviderInterface
{
    protected ?CrudEntityProviderInterface $successor = null;

    final public function processNext(string $classFqcn, array $context = [])
    {
        return $this->successor?->provide($classFqcn, $context);
    }

    final public function setSuccessor(?CrudEntityProviderInterface $successor): void
    {
        $this->successor = $successor;
    }

    public static function getDefaultPriority(): int
    {
        return 1;
    }
}
