<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\State\Provider\CrudEntity;

use Coobix\AdminBundle\Application\UseCase\GetCrudEntityById\Dto\GetCrudEntityByIdInputDto;
use Coobix\AdminBundle\Application\UseCase\GetCrudEntityById\GetCrudEntityById;
use Coobix\AdminBundle\Application\UseCase\SearchCrudEntityByDate\DTO\SearchCrudEntityByDateInputDTO;
use Coobix\AdminBundle\Application\UseCase\SearchCrudEntityByDate\SearchCrudEntityByDate;
use Coobix\AdminBundle\Domain\Exception\ProviderNotFoundException;
use Coobix\AdminBundle\Domain\Exception\ProviderOperationNotFoundException;
use Coobix\AdminBundle\Domain\Model\Admin;

class CrudEntityProvider extends AbstractCrudEntityProvider
{
    public const SUPPORTED_ACTIONS = [
        Admin::ACTION_LIST,
        Admin::ACTION_SHOW,
    ];

    public function __construct(
        private readonly GetCrudEntityById $getCrudEntityByIdUseCase,
        private readonly SearchCrudEntityByDate $searchCrudEntityByDateUseCase
    ) {
    }

    public function provide(string $classFqcn, array $context = [])
    {
        if (!$this->supports($classFqcn, $context)) {
            throw new ProviderNotFoundException(sprintf('Processor for class: [%s] and operation: [%s] not found', $classFqcn, $context['operation']));
        }

        switch ($context['operation']) {
            case Admin::ACTION_SHOW:
                $request = $context['request'];
                $id = $request->attributes->get('id');

                $inputDto = new GetCrudEntityByIdInputDto();
                $inputDto->classFqcn = $classFqcn;
                $inputDto->id = $id;

                return $this->getCrudEntityByIdUseCase->handle($inputDto);
            case Admin::ACTION_LIST:
                return $this->searchCrudEntityByDateUseCase->handle(SearchCrudEntityByDateInputDTO::create($classFqcn));
        }

        throw new ProviderOperationNotFoundException(sprintf('Operation [%s] for provider: [%s] not found', $context['operation'], CrudEntityProvider::class));
    }

    public function supports(string $classFqcn, array $context = []): bool
    {
        return in_array($context['operation'], self::SUPPORTED_ACTIONS);
    }

    public static function getDefaultPriority(): int
    {
        return -1;
    }
}
