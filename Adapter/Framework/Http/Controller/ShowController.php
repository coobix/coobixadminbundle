<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\Controller;

use Coobix\AdminBundle\Adapter\Framework\Http\Factory\AdminFactory;
use Coobix\AdminBundle\Adapter\Framework\Http\Resolver\EntityProviderResolver;
use Coobix\AdminBundle\Domain\Exception\AdminRouteNotfoundException;
use Coobix\AdminBundle\Domain\Model\Admin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\Translation\TranslatorInterface;

class ShowController extends AbstractController
{
    public function __construct(
        protected readonly AdminFactory $factory,
        protected readonly EntityManagerInterface $entityManager,
        protected readonly TranslatorInterface $translator,
        protected readonly EntityProviderResolver $resolver
    ) {
    }

    /**
     * @throws AdminRouteNotfoundException
     */
    public function __invoke(Request $request, $id, string $class): Response
    {
        $admin = $this->factory->create($class);
        if (!in_array(Admin::ACTION_SHOW, $admin->getAllowedActions())) {
            throw new AdminRouteNotfoundException(sprintf('Route not found for:  %s.', $request->getMethod().' '.$request->getPathInfo()));
        }

        $entity = $this->resolver->resolveProvider($admin->getEntityFqcn(), ['request' => $request, 'operation' => Admin::ACTION_SHOW]);

        if (!$entity) {
            throw new NotFoundHttpException($this->translator->trans('messages.resource_not_found', [], 'CoobixAdminBundle'));
        }

        return $this->render($admin->getShowTemplate(), [
            'entity' => $entity,
            'admin' => $admin,
        ]);
    }
}
