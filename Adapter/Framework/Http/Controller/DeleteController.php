<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\Controller;

use Coobix\AdminBundle\Adapter\Framework\Http\Factory\AdminFactory;
use Coobix\AdminBundle\Domain\Exception\AdminRouteNotfoundException;
use Coobix\AdminBundle\Domain\Model\Admin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

class DeleteController extends AbstractController
{
    public function __construct(
        private readonly AdminFactory $factory,
        private readonly ManagerRegistry $managerRegistry,
        private readonly TranslatorInterface $translator
    ) {
    }

    /**
     * @throws AdminRouteNotfoundException
     */
    public function __invoke(Request $request, string $class): RedirectResponse
    {
        $admin = $this->factory->create($class);
        if (!in_array(Admin::ACTION_DELETE, $admin->getAllowedActions())) {
            throw new AdminRouteNotfoundException(sprintf('Route not found for:  %s.', $request->getMethod().' '.$request->getPathInfo()));
        }

        $submittedToken = $request->request->get('token');

        // 'delete-item' is the same value used in the template to generate the token
        if ($this->isCsrfTokenValid('delete-item', $submittedToken)) {
            $id = $request->request->get('id', 0);
            // TODO maybe here you can use a provider
            $repository = new ServiceEntityRepository($this->managerRegistry, $admin->getEntityFqcn());
            $entity = $repository->find($id);

            if (!$entity) {
                throw $this->createNotFoundException($this->translator->trans('messages.resource_not_found', [], 'CoobixAdminBundle'));
            }

            try {
                // TODO maybe here you can use a processor
                $manager = $this->managerRegistry->getManagerForClass($admin->getEntityFqcn());
                $manager->remove($entity);
                $manager->flush();

                $this->addFlash('success', $this->translator->trans('messages.resource_deleted', [], 'CoobixAdminBundle'));
            } catch (\Exception $e) {
                $this->addFlash('danger', $this->translator->trans('messages.resource_not_deleted', [], 'CoobixAdminBundle'));
            }
        }

        return $this->redirect($this->generateUrl('admin_list', ['class' => $admin->getRouteName()]));
    }
}
