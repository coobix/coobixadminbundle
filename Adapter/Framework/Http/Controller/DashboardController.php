<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\Controller;

use Coobix\AdminBundle\Adapter\Framework\Http\Factory\AdminFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class DashboardController extends AbstractController
{
    public function __construct(
        protected readonly AdminFactory $factory,
        protected readonly TranslatorInterface $translator
    ) {
    }

    public function __invoke(Request $request): Response
    {
        $admin = $this->factory->create('dashboard');

        return $this->render($admin->getShowTemplate(), [
            'admin' => $admin,
        ]);
    }
}
