<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\Controller;

use AutoMapper\AutoMapper;
use Coobix\AdminBundle\Adapter\Framework\Http\Factory\AdminFactory;
use Coobix\AdminBundle\Adapter\Framework\Http\Resolver\EntityProcessorResolver;
use Coobix\AdminBundle\Adapter\Framework\Http\Service\EntityMappingService;
use Coobix\AdminBundle\Domain\Exception\AdminRouteNotfoundException;
use Coobix\AdminBundle\Domain\Exception\CrudEntityEditFormDataClassNotFoundException;
use Coobix\AdminBundle\Domain\Model\Admin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfonycasts\MicroMapper\MicroMapperInterface;

class EditController extends AbstractController
{
    protected Admin $admin;
    public function __construct(
        protected readonly AdminFactory $factory,
        protected readonly ManagerRegistry $managerRegistry,
        protected readonly TranslatorInterface $translator,
        protected readonly EntityProcessorResolver $resolver,
        protected AutoMapper $mapper
    ) {
    }

    /**
     * @throws AdminRouteNotfoundException
     */
    public function __invoke(Request $request, string $id, string $class): Response
    {
        $this->admin = $this->factory->create($class);
        $frameId = $request->headers->get('Turbo-Frame');

        if (!in_array(Admin::ACTION_UPDATE, $this->admin->getAllowedActions())) {
            throw new AdminRouteNotfoundException(sprintf('Route not found for:  %s.', $request->getMethod().' '.$request->getPathInfo()));
        }

        // TODO maybe here you can use a provider
        $repository = new ServiceEntityRepository($this->managerRegistry, $this->admin->getEntityFqcn());
        $entity = $repository->find($id);

        if (!$entity) {
            // TODO create a custom Admin exception
            throw $this->createNotFoundException($this->translator->trans('messages.resource_not_found', [], 'CoobixAdminBundle'));
        }

        $formEntity = $this->createEditFormDataClassInstance($entity);

        $form = $this->createForm($this->admin->getEditForm(), $formEntity, [
            'action' => $this->generateUrl('admin_edit', ['class' => $this->admin->getRouteName(), 'id' => $entity->getId()]),
            'method' => 'POST',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->resolver->resolveProcessor($formEntity, [
                    'request' => $request,
                    'operation' => 'update',
                    'entity' => $this->admin->getEntityFqcn(),
                ]);
                $this->addFlash('success', $this->translator->trans('messages.resource_updated', [], 'CoobixAdminBundle'));

                return $this->redirect($this->generateUrl('admin_edit', ['class' => $this->admin->getRouteName(), 'id' => $id]));
            } else {
                $this->addFlash('danger', $this->translator->trans('messages.resource_not_updated', [], 'CoobixAdminBundle'));
            }
        }

        return $this->renderForm($this->admin->getEditTemplate(), [
            'admin' => $this->admin,
            'entity' => $entity,
            'form' => $form,
            'frame_id' => $frameId,
        ]);
    }

    /**
     * This function is responsible for create/return an entity for the Edit form data class.
     *
     * @return object
     */
    protected function createEditFormDataClassInstance($entity): object
    {
        $formClass = $this->admin->getEditForm();
        $formDataClassFqcn = $formClass::getFormDataClassFqcn();
        if (EntityMappingService::getClassName($entity) !== EntityMappingService::getClassName($formDataClassFqcn)) {
            return $this->mapper->map($entity, $formDataClassFqcn);
        }
        return $entity;
    }
}
