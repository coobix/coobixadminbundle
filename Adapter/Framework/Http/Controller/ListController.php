<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\Controller;

use Coobix\AdminBundle\Adapter\Database\ORM\Doctrine\Repository\DoctrineCrudEntityRepository;
use Coobix\AdminBundle\Adapter\Framework\Http\Factory\AdminFactory;
use Coobix\AdminBundle\Adapter\Framework\Http\Resolver\EntityProviderResolver;
use Coobix\AdminBundle\Domain\Exception\AdminRouteNotfoundException;
use Coobix\AdminBundle\Domain\Model\Admin;
use Coobix\AdminBundle\Domain\Model\BaseList;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class ListController extends AbstractController
{
    protected Admin $admin;

    public function __construct(
        protected readonly AdminFactory $factory,
        protected readonly ManagerRegistry $managerRegistry,
        protected readonly PaginatorInterface $paginator,
        protected readonly TranslatorInterface $translator,
        protected readonly DoctrineCrudEntityRepository $crudEntityRepository,
        protected readonly EntityProviderResolver $resolver
    ) {
    }

    /**
     * @throws AdminRouteNotfoundException
     */
    public function __invoke(Request $request, string $class): Response
    {
        $this->admin = $this->factory->create($class);

        if (!in_array(Admin::ACTION_LIST, $this->admin->getAllowedActions())) {
            throw new AdminRouteNotfoundException(sprintf('Route not found for:  %s.', $request->getMethod().' '.$request->getPathInfo()));
        }

        $listSearchForm = $this->createSearchListForm();
        $listSearchForm->handleRequest($request);
        $pagination = $this->getListItems($request, $listSearchForm);

        return $this->render($this->admin->getListTemplate(), [
            'admin' => $this->admin,
            'pagination' => $pagination,
            'listSearchFormIsSubmitted' => $listSearchForm->isSubmitted(),
            'listSearchForm' => $listSearchForm->createView(),
        ]);
    }

    protected function preSetListQuery(Request $request): QueryBuilder
    {

        $qb = $this->resolver->resolveProvider($this->admin->getEntityFqcn(), [
            'request' => $request,
            'operation' => Admin::ACTION_LIST
        ]);

        assert($qb instanceof QueryBuilder);

        return $qb;

    }

    protected function getListItems(Request $request, ?FormInterface $listSearchForm): \Knp\Component\Pager\Pagination\PaginationInterface
    {
        // PRE_SET_BASEQUERY
        $qb = $this->preSetListQuery($request);

        if ($listSearchForm instanceof FormInterface && $listSearchForm->isSubmitted()) {
            $manager = $this->managerRegistry->getManagerForClass($this->admin->getEntityFqcn());
            $list = new BaseList($manager, $this->admin->getEntityFqcn());
            $list->setQb($qb);
            if ($listSearchForm->isValid()) {
                $list->applyFilters();
                $qb = $list->getQb();
            }
        }

        // if page number is < 1 or not a number, set the page number = 1
        $pageNumber = !$request->query->getInt('page', 1) ? 1 : $request->query->getInt('page', 1);

        return $this->paginator->paginate(
            $qb->getQuery(), /* query NOT result */
            $pageNumber, /* page number */
            10 /* limit per page */
        );
    }

    /**
     * Creates a form to create a Page entity.
     *
     * @return \Symfony\Component\Form\FormInterface The form
     */
    protected function createSearchListForm(string $url = ''): \Symfony\Component\Form\FormInterface
    {
        if ('' === $url) {
            $url = $this->generateUrl('admin_list', [
                    'class' => $this->admin->getRouteName()]
            );
        }

        // If the form is not defined create a dummy form
        if (!$this->admin->getListSearchForm()) {
            $formFactory = Forms::createFormFactory();

            return $this->createFormBuilder()
                ->setAction($url)
                ->setMethod('GET')
                ->getForm();
        }

        return $this->createForm($this->admin->getListSearchForm(), null, [
            'action' => $url,
            'method' => 'GET',
        ]);
    }
}
