<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\Controller;

use Coobix\AdminBundle\Adapter\Framework\Http\Factory\AdminFactory;
use Coobix\AdminBundle\Adapter\Framework\Http\Resolver\EntityProcessorResolver;
use Coobix\AdminBundle\Domain\Exception\AdminRouteNotfoundException;
use Coobix\AdminBundle\Domain\Exception\CrudEntityNewFormClassNotFoundException;
use Coobix\AdminBundle\Domain\Model\Admin;
use Coobix\AdminBundle\Domain\Model\AdminInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class NewController extends AbstractController
{

    protected Admin $admin;

    public function __construct(
        protected AdminFactory $factory,
        protected TranslatorInterface $translator,
        protected EntityProcessorResolver $resolver,
        protected SerializerInterface $serializer,
    ) {
    }

    /**
     * @throws AdminRouteNotfoundException
     * @throws CrudEntityNewFormClassNotFoundException
     * @throws \JsonException
     */
    public function __invoke(Request $request, string $class): Response
    {
        $this->setAdmin($class);

        if (!in_array(Admin::ACTION_CREATE, $this->admin->getAllowedActions())) {
            throw new RouteNotFoundException(sprintf('Route not found for: %s %s.', $request->getMethod(), $request->getPathInfo()));
        }

        $formEntity = $this->createNewFormDataClassInstance($request);

        $form = $this->createForm($this->admin->getNewForm(), $formEntity, [
            'action' => $this->generateUrl('admin_new', ['class' => $this->admin->getRouteName()]),
            'method' => 'POST',
            'frameId' => $request->headers->get('Turbo-Frame')
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $this->resolver->resolveProcessor($formEntity, [
                    'request' => $request,
                    'operation' => Admin::ACTION_CREATE,
                    'entity' => $this->admin->getEntityFqcn(),
                ]);

                return $this->redirectOnSuccess($request, $this->admin, $data);

            } else {
                $this->setErrorFlash();
            }
        }

        return $this->renderResponse($request, $this->admin, $form);
    }

    /**
     * This function is responsible for create a new entity for the New form data class
     */
    public function createNewFormDataClassInstance(mixed $data): object
    {
        $formClass = $this->admin->getNewForm();
        $formDataClassFqcn = $formClass::getFormDataClassFqcn();
        return new $formDataClassFqcn();
    }

    protected function setErrorFlash()
    {
        $this->addFlash('danger', $this->translator->trans('messages.resource_not_created', [], 'CoobixAdminBundle'));
    }

    protected function redirectOnSuccess($request, $admin, $data): Response
    {
        $this->addFlash('success', $this->translator->trans('messages.resource_created', [], 'CoobixAdminBundle'));

        return $this->redirect($this->generateUrl('admin_show', [
                'class' => $this->admin->getRouteName(),
                'id' => $data->getId(),
            ]
        ));
    }

    protected function renderResponse($request, $admin, $form): Response
    {
        return $this->renderForm($this->admin->getNewTemplate(), [
            'admin' => $admin,
            'form' => $form,
        ]);
    }

    protected function setAdmin(string $class): void
    {
        $this->admin = $this->factory->create($class);
    }

    protected function getAdmin(): Admin
    {
        return $this->admin;
    }
}
