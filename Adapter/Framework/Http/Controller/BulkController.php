<?php

namespace Coobix\AdminBundle\Adapter\Framework\Http\Controller;

use Coobix\AdminBundle\Adapter\Framework\Http\Factory\AdminFactory;
use Coobix\AdminBundle\Domain\Exception\AdminRouteNotfoundException;
use Coobix\AdminBundle\Domain\Model\Admin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class BulkController extends AbstractController
{
    public function __construct(
        private readonly AdminFactory $factory,
        private readonly ManagerRegistry $managerRegistry,
        private readonly TranslatorInterface $translator
    ) {
    }

    /**
     * @throws AdminRouteNotfoundException
     */
    public function __invoke(Request $request, string $class): Response
    {
        $admin = $this->factory->create($class);

        if (!in_array(Admin::ACTION_BULK_DELETE, $admin->getAllowedActions())) {
            throw new AdminRouteNotfoundException(sprintf('Route not found for:  %s.', $request->getMethod().' '.$request->getPathInfo()));
        }

        $ids = explode(',', $request->request->get('bulkIds'));
        unset($ids[0]);

        $action = $request->request->get('action');

        switch ($action) {
            case 'delete':
                // TODO  cambiar esta consultaS capaz q podes usar un provider
                $repository = new ServiceEntityRepository($this->managerRegistry, $admin->getEntityFqcn());
                $qb = $repository->createQueryBuilder('e');
                $qb->andWhere('e.id IN (:e_ids)');
                $qb->setParameter('e_ids', $ids);
                $entities = $qb->getQuery()->getResult();

                if ($entities) {
                    $result = $this->deleteBulk($entities);
                    if ($result['success'] > 0) {
                        $this->addFlash('success', $this->translator->trans('messages.resources_deleted', ['%count%' => $result['success']], 'CoobixAdminBundle'));
                    }
                    if ($result['error'] > 0) {
                        $this->addFlash('danger', $this->translator->trans('messages.resources_not_deleted', ['%count%' => $result['error']], 'CoobixAdminBundle'));
                    }
                } else {
                    $this->addFlash('danger', $this->translator->trans('messages.resource_not_found', [], 'CoobixAdminBundle'));
                }
                break;
            default:
        }

        return $this->redirect($this->generateUrl('admin_list', [
            'class' => $admin->getRouteName(),
        ]));
    }

    protected function deleteBulk($entities): array
    {
        $result = ['success' => 0, 'error' => 0];
        foreach ($entities as $e) {
            $manager = $this->managerRegistry->getManagerForClass(\get_class($e));

            try {
                $manager->remove($e);
                $manager->flush();

                ++$result['success'];
            } catch (\Exception $e) {
                ++$result['error'];
            }
        }

        return $result;
    }
}
