<?php

namespace Coobix\AdminBundle\Adapter\Database\ORM\Doctrine\Repository;

use Coobix\AdminBundle\Domain\Repository\CrudEntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class DoctrineCrudEntityRepository implements CrudEntityRepository
{
    private readonly ManagerRegistry $managerRegistry;

    public function __construct(
        ManagerRegistry $managerRegistry,
    ) {
        $this->managerRegistry = $managerRegistry;
    }

    public function save(object $crudEntity): void
    {
        $manager = $this->managerRegistry->getManagerForClass(\get_class($crudEntity));
        $manager->persist($crudEntity);
        $manager->flush();
    }

    public function findOneById(string $classFqcn, string $id): object|null
    {
        $repository = new ServiceEntityRepository($this->managerRegistry, $classFqcn);

        return $repository->find($id);
    }

    public function searchByDate(string $classFqcn): QueryBuilder
    {
        $repository = new ServiceEntityRepository($this->managerRegistry, $classFqcn);
        $qb = $repository->createQueryBuilder('e');
        $qb->orderBy('e.createdAt', 'DESC');

        return $qb;
    }
}
