<?php

namespace Coobix\AdminBundle;

use Coobix\AdminBundle\DependencyInjection\Compiler\StateProcessorCompilerPass;
use Coobix\AdminBundle\DependencyInjection\Compiler\StateProviderCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CoobixAdminBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new StateProviderCompilerPass());
        $container->addCompilerPass(new StateProcessorCompilerPass());
    }
}
