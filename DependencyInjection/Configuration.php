<?php

namespace Coobix\AdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('coobix_admin');
        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('theme')->defaultValue('metronic')->end()
                ->scalarNode('registration_front')->defaultValue('true')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
