<?php

namespace Coobix\AdminBundle\DependencyInjection\Compiler;

use Coobix\AdminBundle\Adapter\Framework\Http\State\Provider\CrudEntity\CrudEntityProviderInterface;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class StateProviderCompilerPass implements CompilerPassInterface
{
    /**
     * @throws \Exception
     */
    public function process(ContainerBuilder $container)
    {
        // Get all registered providers
        $stateProviders = $container->findTaggedServiceIds('coobix_admin.crud_entity_provider');

        // if there are no providers return
        if (count($stateProviders) < 1) {
            return;
        }

        $providersByPriority = [];
        foreach ($stateProviders as $id => $attributes) {
            /** @var CrudEntityProviderInterface $providerClass */
            $providerClass = $container->getDefinition($id)->getClass();
            $providersByPriority[$providerClass::getDefaultPriority()][] = $id;
        }
        $sortedHandlers = $this->sortHandlers($providersByPriority);

        $firstProvidersServiceName = array_shift($sortedHandlers);
        $firstHandler = new Alias($firstProvidersServiceName);
        $firstHandler->setPublic(true);
        $container->setAlias('app.coobix_admin.crud_entity_provider', $firstHandler);
        $handlerDefinition = $container->getDefinition($firstProvidersServiceName);

        foreach ($sortedHandlers as $handler) {
            $handler = $container->getDefinition($handler);
            $handlerDefinition->addMethodCall('setSuccessor', [$handler]);
            $handlerDefinition = $handler;
        }
    }

    /**
     * @return string[]
     */
    private function sortHandlers(array $providersByPriority): array
    {
        \krsort($providersByPriority);

        return array_merge(...$providersByPriority);
    }
}
