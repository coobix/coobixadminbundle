<?php

namespace Coobix\AdminBundle\DependencyInjection\Compiler;

use Coobix\AdminBundle\Adapter\Framework\Http\State\Processor\CrudEntity\CrudEntityProcessorInterface;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class StateProcessorCompilerPass implements CompilerPassInterface
{
    /**
     * @throws \Exception
     */
    public function process(ContainerBuilder $container)
    {
        // Get all registered processors
        $stateProcessors = $container->findTaggedServiceIds('coobix_admin.crud_entity_processor');

        // if there are no processors return
        if (count($stateProcessors) < 1) {
            return;
        }

        $processorsByPriority = [];
        foreach ($stateProcessors as $id => $attributes) {
            /** @var CrudEntityProcessorInterface $processorClass */
            $processorClass = $container->getDefinition($id)->getClass();
            $processorsByPriority[$processorClass::getDefaultPriority()][] = $id;
        }
        $sortedHandlers = $this->sortHandlers($processorsByPriority);

        $firstProcessorsServiceName = array_shift($sortedHandlers);
        $firstHandler = new Alias($firstProcessorsServiceName);
        $firstHandler->setPublic(true);
        $container->setAlias('app.coobix_admin.crud_entity_processor', $firstHandler);
        $handlerDefinition = $container->getDefinition($firstProcessorsServiceName);

        foreach ($sortedHandlers as $handler) {
            $handler = $container->getDefinition($handler);
            $handlerDefinition->addMethodCall('setSuccessor', [$handler]);
            $handlerDefinition = $handler;
        }
    }

    /**
     * @return string[]
     */
    private function sortHandlers(array $processorsByPriority): array
    {
        \krsort($processorsByPriority);

        return array_merge(...$processorsByPriority);
    }
}
