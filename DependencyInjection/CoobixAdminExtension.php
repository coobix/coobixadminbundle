<?php

namespace Coobix\AdminBundle\DependencyInjection;

use Coobix\AdminBundle\Adapter\Framework\Http\State\Processor\CrudEntity\CrudEntityProcessorInterface;
use Coobix\AdminBundle\Adapter\Framework\Http\State\Provider\CrudEntity\CrudEntityProviderInterface;
use Coobix\AdminBundle\Domain\Model\Admin;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class CoobixAdminExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        // Register Admin model classes
        $container->registerForAutoconfiguration(Admin::class)
            ->addTag('coobix_admin.admin')
        ;

        // Register Entity providers
        $container->registerForAutoconfiguration(CrudEntityProviderInterface::class)
            ->addTag('coobix_admin.crud_entity_provider')
        ;

        // Register Entity Processors
        $container->registerForAutoconfiguration(CrudEntityProcessorInterface::class)
            ->addTag('coobix_admin.crud_entity_processor')
        ;

        $loader = new XmlFileLoader($container, new FileLocator(dirname(__DIR__).'/config'));
        $loader->load('services.xml');
    }
}
