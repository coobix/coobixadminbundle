

module.exports = {
    'testRegex': 'test/.*\\.test.js',
    'setupFilesAfterEnv': ['./assets/test/setup.js'],
    moduleFileExtensions: ['js', 'jsx'],
    moduleDirectories: ['node_modules', 'bower_components', 'shared'],



};