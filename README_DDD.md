# Hexagonal base structure


```bash
##ADAPTER##
#Admin
mkdir MODULE/Adapter/Admin/Adapter/Framework/Controller
mkdir MODULE/Adapter/Admin/Adapter/Framework/State/Processor
mkdir MODULE/Adapter/Admin/Adapter/Framework/State/Provider
mkdir MODULE/Adapter/Admin/Domain/Model
#Api Platform
#Database
mkdir MODULE/Adapter/Database/ORM/Doctrine/mappings/ValueObject
mkdir MODULE/Adapter/Database/ORM/Doctrine/Migrations
mkdir MODULE/Adapter/Database/ORM/Doctrine/Repository
#Framework
mkdir MODULE/Adapter/Framework/config
mkdir MODULE/Adapter/Framework/Http/Controller
mkdir MODULE/Adapter/Framework/templates
mkdir MODULE/Adapter/Framework/translations

##APPLICATION##
mkdir MODULE/Application/Domain
mkdir MODULE/Application/UseCase

##DOMAINN##
mkdir MODULE/Domain/Exception
mkdir MODULE/Domain/Model/ValueObject
mkdir MODULE/Domain/Repository



```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)