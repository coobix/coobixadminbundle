# CoobixAdminBundle
VERSION: 0.0.1
AdminBundle is a symfony bundle for dealing with quick CRUDs.

## Installation

Use composer manager to install this bundle.

```bash
composer install coobix/admin-bundle
```

## Routes

config/routes.yaml

```yaml
#..
coobix_admin:
    resource: '@CoobixAdminBundle/config/routes.yaml'
    prefix: 'admin'
```
## Enable

config/bundles.php
```php
return [
    //..
    Coobix\AdminBundle\CoobixAdminBundle::class => ['all' => true],
    Knp\Bundle\PaginatorBundle\KnpPaginatorBundle::class => ['all' => true],
];
```

## Translations

config/services.yaml

```yaml
#..
parameters:
  APP_SUPPORTED_LOCALES: [ 'en', 'es' ]
  APP_LOCALE: es

twig:
  globals:
    APP_SUPPORTED_LOCALES: '%APP_SUPPORTED_LOCALES%'
```

```yaml
set_locale:
  path: /set_locale/{newLocale}
  controller: NAME\SPACE\SetLocaleController
  methods: [ GET ]
```

```php
<?php

namespace //...

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SetLocaleController extends AbstractController
{
    public function __invoke(Request $request, string $newLocale): Response
    {
        $request->getSession()->set('_locale', $newLocale);

        return $this->redirectToRoute('home_page', [
            '_locale' => $newLocale
        ]);
    }
}
```

```php
<?php

namespace //..

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LocaleSubscriber implements EventSubscriberInterface
{
    private string $defaultLocale;

    public function __construct(string $defaultLocale = 'es')
    {
        $this->defaultLocale = $defaultLocale;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->hasPreviousSession()) {
            return;
        }

        // try to see if the locale has been set as a _locale routing parameter
        if ($locale = $request->attributes->get('_locale')) {
            $request->getSession()->set('_locale', $locale);
        } else {
            // if no explicit locale has been set on this request, use one from the session
            $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            // must be registered before (i.e. with a higher priority than) the default Locale listener
            KernelEvents::REQUEST => [['onKernelRequest', 20]],
        ];
    }
}
```



## Create Admin Model

Entity: Workflow
src/MODULE_NAME/Adapter/Admin/Domain/Model/WorkflowAdmin.php

```php
<?php

namespace Workflow\Adapter\Admin\Domain\Model;


use App\Workflow\Adapter\Framework\Http\Form\WorkflowEditType;
use App\Workflow\Adapter\Framework\Http\Form\WorkflowListSearchType;
use App\Workflow\Adapter\Framework\Http\Form\WorkflowType;
use Coobix\AdminBundle\Domain\Model\Admin;
use Workflow\Domain\Model\Workflow;

class WorkflowAdmin extends Admin
{

    protected $newForm = WorkflowType::class;
    protected $editForm = WorkflowEditType::class;
    protected $listSearchForm = WorkflowListSearchType::class;
    protected array $listColumns = ['name'];
    protected array $showRows = ['name'];
    protected string $showTemplate = '@Workflow/admin/workflow/show.html.twig';

    public static function getEntityFqcn(): string
    {
       return Workflow::class;
    }

    public static function getRouteName(): string
    {
        return 'workflow';
    }
}
```


## Create Crud Entity Processor

Entity: Product
src/MODULE_NAME/Adapter/Admin/Adapter/Framework/State/Processor/ProductProcessor.php
```php
<?php
    
    use Coobix\AdminBundle\Adapter\Framework\Http\State\Processor\CrudEntity\AbstractCrudEntityProcessor;
    use Symfony\Component\Serializer\SerializerInterface;
    use Symfony\Component\Uid\Uuid;

    class ProductProcessor extends AbstractCrudEntityProcessor
        {
        public function __construct(
            public readonly SerializerInterface $serializer
        )
        {
        }
    
        public function process(object $crudEntity, array $context = []):?string
        {
            if (!$this->supports($crudEntity, $context)) {
                return $this->processNext($crudEntity, $context);
            }
            
            $useCaseResponseDTO = //.. Use case handler
            
            return $this->serializer->serialize($useCaseResponseDTO, 'json');
        }
    
        public function supports(object $crudEntity, array $context = []): bool
        {
            return \get_class($crudEntity) === Product::class;
        }
    }
```

## Create Crud Entity Provider

Entity: Product
src/MODULE_NAME/Adapter/Admin/Adapter/Framework/State/Provider/ProductProvider.php
```php
<?php

namespace //..
use Coobix\AdminBundle\Adapter\Framework\Http\State\Provider\CrudEntity\AbstractCrudEntityProvider;
use Symfony\Component\HttpFoundation\Request;

class OceanScheduleSearchProvider extends AbstractCrudEntityProvider
{

    public function __construct
    (
    )
    {
    }

    public function provide(string $classFqcn, array $context = [])
    {
        if (!$this->supports($classFqcn, $context)) {
            return $this->processNext($classFqcn, $context);
        }

        return //.. Use case handler
    }

    public function supports(string $classFqcn, array $context = []): bool
    {
        return $classFqcn === Product::class;
    }
}
```


## FORMS
```php
<?php

namespace OceanSchedule\Adapter\Framework\Form;

use OceanSchedule\Domain\Model\BookingRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class BookingRequestType extends AbstractType
{
    const CONTROLLER_NAME = 'booking-request-form';

    public function __construct(
        protected readonly TranslatorInterface $translator
    )
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                    'label' => $this->translator->trans("messages.name"),
                    'attr' => [
                        'data-' . self::CONTROLLER_NAME . '-target' => "nameInput"
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BookingRequest::class,
        ]);
    }
}
```


## NEW CONTROLLER WITH DTO
```php

//1. Create the DTO: MODULE\Adapter\Framework\Http\DTO

<?php

namespace Workflow\Adapter\Framework\Http\DTO;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Workflow\Domain\Model\Client;
use Workflow\Domain\Model\ValueObject\ClientName;

/**
 * This class is responsible for hold data and validation of a client create request
 */
class CreateClientRequestDTO
{

    public ?string $name;

    public static function loadValidatorMetadata(ClassMetadata $metadata): void
    {
        foreach (ClientName::loadValidationConstraints() as $constraint) {
            $metadata->addPropertyConstraint('name', $constraint);
        }
    }

    public static function fromModel(Client $model): self
    {
        $dto = new self();
        $dto->name = $model->getName();
        return $dto;
    }

}


//2. Change de form data class
//..
public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CreateClientRequestDTO::class,
        ]);
    }



```


<?php

use Coobix\AdminBundle\Service\AdminManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\Kernel;

class FunctionalTest extends TestCase
{
    public function testServiceWiring()
    {
        $kernel = new CoobixAdminTestingKernel('test', true);
        $kernel->boot();
        $container = $kernel->getContainer();

        $adminManager = $container->get('coobix_admin.service.admin_manager');
        $this->assertInstanceOf(AdminManager::class, $adminManager);
    }
}

class CoobixAdminTestingKernel extends Kernel
{
    public function registerBundles()
    {
        return [
            new \Coobix\AdminBundle\CoobixAdminBundle(),
        ];
    }

    public function registerContainerConfiguration(Symfony\Component\Config\Loader\LoaderInterface $loader)
    {
        // TODO: Implement registerContainerConfiguration() method.
    }
}


### DEV utils

```bash
./vendor/bin/phpstan analyse ../coobixadminbundle/ -c ../coobixadminbundle/phpstan.neon -l 3
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)