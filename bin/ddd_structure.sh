#!/bin/bash
#https://unix.stackexchange.com/questions/31414/how-can-i-pass-a-command-line-argument-into-a-shell-script

helpFunction()
{
   echo ""
   echo "Usage: $0 -a parameterA -b parameterB -c parameterC"
   echo -a "\t-a Directory where the structure has to be created"
   exit 1 # Exit script after printing help
}

while getopts "a:b:c:" opt
do
   case "$opt" in
      a ) parameterA="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$parameterA" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi

# Begin script in case all parameters are correct
echo "$parameterA/Adapter/Admin/Adapter/Framework/Controller"


##ADAPTER##
#Admin
mkdir -p $parameterA/Adapter/Admin/Adapter/Framework/Controller
mkdir -p $parameterA/Adapter/Admin/Adapter/Framework/State/Processor
mkdir -p $parameterA/Adapter/Admin/Adapter/Framework/State/Provider
mkdir -p $parameterA/Adapter/Admin/Domain/Model
#Api Platform
#Database
mkdir -p $parameterA/Adapter/Database/ORM/Doctrine/mappings/ValueObject
mkdir -p $parameterA/Adapter/Database/ORM/Doctrine/Migrations
mkdir -p $parameterA/Adapter/Database/ORM/Doctrine/Repository
#Framework
mkdir -p $parameterA/Adapter/Framework/config
mkdir -p $parameterA/Adapter/Framework/Form
mkdir -p $parameterA/Adapter/Framework/Http/Controller
mkdir -p $parameterA/Adapter/Framework/templates
mkdir -p $parameterA/Adapter/Framework/translations

##APPLICATION##
mkdir -p $parameterA/Application/Domain
mkdir -p $parameterA/Application/UseCase

##DOMAINN##
mkdir -p $parameterA/Domain/Exception
mkdir -p $parameterA/Domain/Model
mkdir -p $parameterA/Domain/Repository
