<?php

namespace Coobix\AdminBundle\Factory;

interface CrudEntityFactoryInterface
{
    public static function createFromCreateDto($dto): object;
}
