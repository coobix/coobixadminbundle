const Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('./Resources/public/')
    .setPublicPath('/public')
    .setManifestKeyPrefix('bundles/coobixadmin')

    //.cleanupOutputBeforeBuild()
    .enableSassLoader()
    .enableSourceMaps(false)
    .enableVersioning(false)
    .disableSingleRuntimeChunk()

    // enables Sass/SCSS support
    .enableSassLoader()


    // uncomment if you use TypeScript
    .enableTypeScriptLoader()

    /*
    // copy FontAwesome fonts
    .copyFiles({
        from: './node_modules/@fortawesome/fontawesome-free/webfonts/',
        // relative to the output dir
        to: 'fonts/[name].[hash].[ext]'
    })
*/
    // copy flag images for country type
    .copyFiles({
        from: './assets/images/',
        to: 'images/[path][name].[ext]'
    })


    .cleanupOutputBeforeBuild()


    // enables the Symfony UX Stimulus bridge (used in assets/bootstrap.js)
    .enableStimulusBridge('./assets/metronic/js/controllers.json')


    .addEntry('app_metronic', './assets/metronic/js/app.js')
    .addEntry('dashboard_metronic', './assets/metronic/js/dashboard.js')
    .addEntry('edit_metronic', './assets/metronic/js/edit.js')
    .autoProvidejQuery()


Encore.configureLoaderRule('typescript', (rule) => {
	//delete rule.exclude;
	rule.exclude = /node_modules\/(?!coobix)/
})

module.exports = Encore.getWebpackConfig()
