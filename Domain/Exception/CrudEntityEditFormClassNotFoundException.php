<?php

namespace Coobix\AdminBundle\Domain\Exception;

class CrudEntityEditFormClassNotFoundException extends \Exception
{
    public static function create(): self
    {
        return new self(
            'Admin\'s Edit Controller needs a form class to be used in the new page: 
            You can set in this way:
            protected function configureCrud(): void
            {
                $this->setEditForm(EditForm::class);
            }'
        );
    }
}
