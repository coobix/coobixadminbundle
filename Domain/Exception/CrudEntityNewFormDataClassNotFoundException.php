<?php

namespace Coobix\AdminBundle\Domain\Exception;

class CrudEntityNewFormDataClassNotFoundException extends \Exception
{
    public static function create(): self
    {
        return new self(
            'Admin is not able to know how to instantiate the class used in the Create form data_class property. 
            Please set the $newFormDataClass property with the corresponding class inside Admin class.'
        );
    }
}
