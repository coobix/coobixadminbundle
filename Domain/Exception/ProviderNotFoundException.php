<?php

namespace Coobix\AdminBundle\Domain\Exception;

class ProviderNotFoundException extends \RuntimeException
{
}
