<?php

namespace Coobix\AdminBundle\Domain\Exception;

// TODO see if can extend from sf route not found
// When triend to extend from sf route not found, the status code is still 500
// 404 expected
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class AdminRouteNotfoundException extends RouteNotFoundException
{
}
