<?php

namespace Coobix\AdminBundle\Domain\Exception;

class CrudEntityArgumentCountException extends \ArgumentCountError
{
    public static function createFromClass(string $entityFqcn): self
    {
        return new self(sprintf(
            'Too few arguments for %s construct method.
            Seems that the class you want to instantiate in the new form has a construct method with
            arguments. 
            In Order to use the new controller you should use a DTO class instead an entity in the form.
            
            1. Create the DTO: MODULE\Adapter\Framework\Http\DTO
            2. Use the created class in the form data_class
            3.
            protected function configureCrud(): void
            {
                $this->setNewFormDataClassFqcn(DTO::class);
            }
            
            In order to let know the Admin how to instantiate this class: 
            
            4. Create a factory that implements CrudEntityFactoryInterface
            5. Configure the Admin
            protected function configureCrud(): void
            {
                $this->setNewFormDataClassFactoryFqcn(YourClassFactory::class);
            }
            ', $entityFqcn));
    }
}
