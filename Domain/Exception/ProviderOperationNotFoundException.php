<?php

namespace Coobix\AdminBundle\Domain\Exception;

class ProviderOperationNotFoundException extends \RuntimeException
{
}
