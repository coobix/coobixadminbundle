<?php

namespace Coobix\AdminBundle\Domain\Exception;

class ProcessorOperationNotFoundException extends \RuntimeException
{
}
