<?php

namespace Coobix\AdminBundle\Domain\Exception;

class CrudEntityEditFormDataClassNotFoundException extends \Exception
{
    public static function create(): self
    {
        return new self(
            'Admin is not able to guess the class used in the Edit form data_class property. 
            Please set the $editFormDataClass property with the corresponding class inside Admin class.'
        );
    }
}
