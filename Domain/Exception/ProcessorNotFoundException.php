<?php

namespace Coobix\AdminBundle\Domain\Exception;

class ProcessorNotFoundException extends \RuntimeException
{
}
