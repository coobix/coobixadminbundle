<?php

namespace Coobix\AdminBundle\Domain\Exception;

class CrudNotFoundException extends \RuntimeException
{
}
