<?php

namespace Coobix\AdminBundle\Domain\Model;

use Symfony\Component\HttpFoundation\Request;

class BaseList
{
    private $request;
    private $entityManager;
    private $class;
    protected $qb;
    protected $em;
    protected $listMaxResults = 10;
    protected $entities;
    protected $title;

    protected $url;

    protected $fields;

    public function __construct($entityManager, $class, $qb = null)
    {
        $this->request = Request::createFromGlobals();
        $this->entityManager = $entityManager;
        $this->class = $class;
        $this->qb = $qb;
    }

    public function getQueryString()
    {
        return $this->request->getQueryString();
    }

    public function setEm()
    {
        $this->em = $this->entityManager;

        return $this;
    }

    public function setListUrl($url)
    {
        $this->url = $url;
    }

    public function setQb($qb)
    {
        $this->qb = $qb;
    }

    public function getQb()
    {
        return $this->qb;
    }

    public function getEntities()
    {
        return $this->entities;
    }

    // CREA LA CONSULTA DEL LISTADO
    // ESTA NO ESTA FUNCIONANDO
    /*
      public function setListQuery() {

      $this->createFormFiltersClause();
      $this->createJoinClause();
      //$this->createLeftJoinClause();
      $this->createOrderClause();
      //$this->qb->setFirstResult($this->getListOffSet());
      //$this->qb->setMaxResults($this->listMaxResults);
      }

     */

    // APLICA FILTROS EN LA CONSULTA
    public function applyFilters()
    {
        $this->createFormFiltersClause();
        $this->createJoinClause();
        // $this->createLeftJoinClause();
        $this->createOrderClause();
    }

    // APLICA ORDEN EN LA CONSULTA
    public function applyOrder()
    {
        $this->createOrderClause();
    }

    // APLICA LIMITES EN LA CONSULTA
    public function applyLimits()
    {
        $this->qb->setFirstResult($this->getListOffSet());
        $this->qb->setMaxResults($this->listMaxResults);
    }

    public function createJoinClause()
    {
        $em = $this->entityManager;
        $classMetaData = $em->getClassMetadata($this->class);

        $rfClass = $classMetaData->getReflectionClass();

        $listSearchFormName = strtolower('list_search');
        if ($this->request->query->has($listSearchFormName)) {
            $formFilters = $this->getFormFilters();

            // $aliasAscii es la letra "a" pero en codigo ascii
            // es decir el 97 = a. Esto es para ir cambiando a->b->c con el fin
            // de que no sean iguales los identificadores de los paramtros
            $aliasAscii = 97;
            foreach ($formFilters as $k => $v) {
                if ('' == $v) {
                    continue;
                }
                try {
                    /*
                      Array (
                      [fieldName] => tipoResolucion
                      [type] => integer
                      [scale] => 0
                      [length] =>
                      [unique] =>
                      [nullable] =>
                      [precision] => 0
                      [columnName] => tipoResolucion
                      )
                     */
                    $field = $classMetaData->getAssociationMapping($k);

                    $this->qb->join('e.'.$field['fieldName'], chr($aliasAscii), 'WITH', chr($aliasAscii).'.id = :'.chr($aliasAscii).'_id', chr($aliasAscii).'.id');
                    $this->qb->setParameter(chr($aliasAscii).'_id', $v);

                    ++$aliasAscii;
                    if (101 == $aliasAscii) {
                        ++$aliasAscii;
                    }
                } catch (\Doctrine\ORM\Mapping\MappingException $exc) {
                    // continue;
                }
            }
        }

        return true;
    }

    private function getFormFilters(string $formName = 'list_search'): array
    {
        return $this->request->query->all()[$formName];
    }

    /*
     * Crea los filtros de la consulta del listado,
     * cuando utilizan el formulario de
     * busqueda avanzada.
     */
    public function createFormFiltersClause()
    {
        // Si no enviaron el formulario
        $listSearchFormName = strtolower('list_search');
        if (!$this->request->query->has($listSearchFormName)) {
            return true;
        }

        // Si lo utilizaron
        // Guardo los filtros
        $formFilters = $this->getFormFilters();

        // Traigo el Entity Manager
        $em = $this->entityManager;
        $classMetaData = $em->getClassMetadata($this->class);
        $rfClass = $classMetaData->getReflectionClass();

        // Empiezo a recorrer los filtros que enviaron.
        // Ej: ?edad=10
        // k: edad, v: 10
        foreach ($formFilters as $k => $v) {
            // Si el filtro no tiene valor, sigue.
            if ('' == $v) {
                continue;
            }

            // Si tiene una valor el filtro
            // intento recuperar la propiedad del objeto.
            try {
                /*
                  Array (
                  [fieldName] => tipoResolucion
                  [type] => integer
                  [scale] => 0
                  [length] =>
                  [unique] =>
                  [nullable] =>
                  [precision] => 0
                  [columnName] => tipoResolucion
                  )
                 */
                $fieldMapping = $classMetaData->getFieldMapping($k);
            } catch (\Doctrine\ORM\Mapping\MappingException $exc) {
                continue;
            }

            switch ($fieldMapping['type']) {
                case 'string':
                case 'text':
                    $cs = 'e.'.$k.' LIKE :e_'.$k.'';
                    $v = '%'.$v.'%';
                    $this->qb->andWhere($cs);
                    $this->qb->setParameter('e_'.$k, $v);
                    break;
                case 'integer':
                case 'float':
                    $cs = 'e.'.$k.' = :e_'.$k;
                    $this->qb->andWhere($cs);
                    $this->qb->setParameter('e_'.$k, $v);
                    break;
                case 'datetime':
                    $cs = ' e.'.$k.' >= :e_desde_'.$k;
                    // 31-01-2015 = dd-mm-aaaa
                    $fechaString = $v;

                    $desde = new \DateTime($fechaString.' 00:00:00');
                    $this->qb->andWhere($cs);
                    $this->qb->setParameter('e_desde_'.$k, $desde);

                    $cs = ' e.'.$k.' <= :e_hasta_'.$k;
                    // 31-01-2015 = dd-mm-aaaa
                    $hasta = new \DateTime($fechaString.' 23:59:59');
                    // $hasta->setDate($fechaArray[2], $fechaArray[0], $fechaArray[1]);
                    // $hasta->add(new \DateInterval('PT23H59M59S'));
                    $this->qb->andWhere($cs);
                    $this->qb->setParameter('e_hasta_'.$k, $hasta);

                    break;

                case 'date':
                    // La fecha viene separada en 3 campos.var_dump($v);
                    $year = (isset($v['year'])) ? $v['year'] : '00';
                    $month = (isset($v['month'])) ? $v['month'] : '00';
                    $day = (isset($v['day'])) ? $v['day'] : '0000';
                    // var_dump($this->request->query->all());
                    // Me fijo si existe el campo dateTo (fechaTo)
                    // comprobantefe[id]

                    try {
                        $dateTo = $this->request->query->get('list_search['.$fieldMapping['fieldName'].'To]', null, true);

                        if ($dateTo) {
                            $v = new \DateTime($year.'-'.$month.'-'.$day);
                            $cs = ' e.'.$k.' >= :e_'.$k;
                            $this->qb->andWhere($cs);
                            $this->qb->setParameter('e_'.$k, $v);

                            $yearTo = (isset($dateTo['year'])) ? $dateTo['year'] : '00';
                            $monthTo = (isset($dateTo['month'])) ? $dateTo['month'] : '00';
                            $dayTo = (isset($dateTo['day'])) ? $dateTo['day'] : '0000';
                            $vTo = new \DateTime($yearTo.'-'.$monthTo.'-'.$dayTo);
                            $cs = ' e.'.$k.' <= :e_'.$k.'To';
                            $this->qb->andWhere($cs);
                            $this->qb->setParameter('e_'.$k.'To', $vTo);
                        } else {
                            $v = new \DateTime($year.'-'.$month.'-'.$day);
                            $cs = ' e.'.$k.' = :e_'.$k;
                            $this->qb->andWhere($cs);
                            $this->qb->setParameter('e_'.$k, $v);
                        }
                    } catch (\Exception $exc) {
                    }

                    break;
            }
        }
    }

    public function createOrderClause()
    {
        // ME FIJO SI ESTAN ORDENANDO CON LOS LINKS DEL LISTADO
        if ($this->request->query->has('_sortBy')) {
            $sortBy = $this->request->query->get('_sortBy');

            // ME FIJO SI EL CAMPO QUE MANDARON POR GET ES ALGUNO DE LOS QUE FILTRA
            /*
              foreach ($this->fields as $f) {
              if ($f->getName() === $sortBy) {
              $orderByField = $f;
              break;
              }
              }

             */

            $orderByField = $sortBy;

            // SI EXISTE EL CAMPO
            if (isset($orderByField)) {
                // ME FIJO SI EXISTE EL ORDEN EN QUE SE ORDENA
                if ($this->request->query->has('_sortOrd')) {
                    $sortOrd = $this->request->query->get('_sortOrd');
                    // ME FIJO SI ES ASC O DESC

                    if ('ASC' != $sortOrd) {
                        $sortOrd = 'DESC';
                    }

                    $this->qb->orderBy('e.'.$orderByField, $sortOrd);
                }

                // AGREGO EL ORDER BY
                /*
                  if ($orderByField->getType() == 'entity') {
                  $this->qb->orderBy('f.' . 'name', $sortOrd);
                  } else {
                  $this->qb->orderBy('e.' . $orderByField->getName(), $sortOrd);
                  }
                 *
                 */
            }
        }

        return $this;
    }

    // RETORNA LAS ENTIDADES DE LA CONSULTA
    public function getResult()
    {
        $this->entities = $this->qb->getQuery()->getResult();

        return $this;
    }

    public function getListOffSet()
    {
        $listOffSet = ($this->getListPage() * $this->listMaxResults) - $this->listMaxResults;

        return $listOffSet;
    }

    public function getListPage()
    {
        $page = 1;
        if ($this->request->query->has('_page')) {
            $page = $this->request->query->get('_page');
        }

        return $page;
    }

    public function getListMaxResults()
    {
        return $this->listMaxResults;
    }

    public function createLeftJoinClause()
    {
        $sortBy = $this->request->query->get('_sortBy');
        // ME FIJO SI EL CAMPO QUE MANDARON POR GET ES ALGUNO DE LOS QUE FILTRA
        foreach ($this->fields as $f) {
            if ($f->getName() === $sortBy) {
                // ME FIJO SI ES UN CAMPO DE TIPO ENTIDAD
                if ('entity' === $f->getType()) {
                    $orderByField = $f;
                    break;
                }
            }
        }
        // SI EXISTE EL CAMPO
        if (isset($orderByField)) {
            $this->qb->leftJoin('e.'.$orderByField->getName(), 'f');
        }
    }

    public function getColFilterUrl($fieldName)
    {
        $urlGetParams = $this->request->query->all();

        if (isset($urlGetParams['_sortBy']) && $urlGetParams['_sortBy'] === $fieldName) {
            $urlGetParams['_sortOrd'] = ('ASC' == $urlGetParams['_sortOrd']) ? 'DESC' : 'ASC';
        } else {
            $urlGetParams['_sortBy'] = $fieldName;
            $urlGetParams['_sortOrd'] = 'ASC';
        }

        return '?'.http_build_query($urlGetParams, '', '&', PHP_QUERY_RFC3986);
        // return '?' . http_build_query($urlGetParams, '', '&');
    }
}
