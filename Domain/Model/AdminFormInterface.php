<?php

namespace Coobix\AdminBundle\Domain\Model;

interface AdminFormInterface
{
    public static function getFormDataClassFqcn(): ?string;

    public static function getFormControllerName(): string;
}