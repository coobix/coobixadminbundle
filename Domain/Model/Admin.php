<?php

namespace Coobix\AdminBundle\Domain\Model;

use Coobix\AdminBundle\Adapter\Framework\Http\Service\EntityMappingService;
use Coobix\AdminBundle\Domain\Exception\CrudEntityArgumentCountException;
use Coobix\AdminBundle\Domain\Exception\CrudEntityEditFormClassNotFoundException;
use Coobix\AdminBundle\Domain\Exception\CrudEntityNewFormClassNotFoundException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfonycasts\MicroMapper\MicroMapperInterface;

/**
 * Admin.
 */
abstract class Admin
{
    public const ACTION_LIST = 'list';
    public const ACTION_CREATE = 'create';
    public const ACTION_UPDATE = 'update';
    public const ACTION_DELETE = 'delete';
    public const ACTION_SHOW = 'show';
    public const ACTION_BULK_DELETE = 'bulk_delete';

    // TODO on twig templates, check if these actions are allowed, if yes display action buttons
    private array $allowedActions = [
        self::ACTION_LIST,
        self::ACTION_CREATE,
        self::ACTION_UPDATE,
        self::ACTION_DELETE,
        self::ACTION_SHOW,
        self::ACTION_BULK_DELETE,
    ];

    protected string|null $entityFactory = null;

    // LIST
    private string $listTitle = '';
    private string $listTemplate = '@CoobixAdmin/metronic/Admin/list.html.twig';
    private string|null $listSearchForm = null;

    protected string $listDeleteFormTemplate = '@CoobixAdmin/metronic/Admin/list_delete_form.html.twig';
    private array $listColumns = [];

    // SHOW
    private string $showTemplate = '@CoobixAdmin/metronic/Admin/show.html.twig';
    protected string $showTitle = '';
    private array $showRows = [];

    // NEW
    private string $newTemplate = '@CoobixAdmin/metronic/Admin/new.html.twig';
    private string|null $newForm = null;
    /**
     * The new form data class Fqcn.
     */
    private string|null $newFormDataClass = null;

    /**
     * The new form data class Factory Fqcn.
     */
    private string|null $newFormDataClassFactory = null;
    private string $newTitle = '';

    // EDIT
    private string $editTemplate = '@CoobixAdmin/metronic/Admin/edit.html.twig';
    private $editForm;

    private string|null $editFormDataClassFqcn = null;
    private string $editTitle = '';

    public function __construct(
        protected readonly EntityMappingService $entityMappingService,
        protected readonly TranslatorInterface $translator,
        protected readonly ManagerRegistry $managerRegistry,
        protected readonly MicroMapperInterface $mapper
    ) {
        $this->configureCrud();
    }

    private function ensureAdminConfigIsValid(): void
    {
        // TODO
        // validate that the newFormDataClass === form data_class set
    }

    /*******
     * NEW *
     *******/

    public function setNewTitle(string $newTitle): self
    {
        $this->newTitle = $newTitle;

        return $this;
    }

    public function setNewTemplate(string $newTemplate): self
    {
        $this->newTemplate = $newTemplate;

        return $this;
    }

    public function getNewTemplate(): string
    {
        return $this->newTemplate;
    }

    public function setNewForm(string $newForm): self
    {
        if (!in_array(AdminFormInterface::class, class_implements($newForm))) {
            throw new \Exception('Form class must implement AdminFormInterface');
        }

        $this->newForm = $newForm;

        return $this;
    }

    /**
     * @throws CrudEntityNewFormClassNotFoundException
     */
    public function getNewForm(): string
    {
        if (!$this->newForm) {
            throw CrudEntityNewFormClassNotFoundException::create();
        }

        return $this->newForm;
    }

    /********
     * EDIT *
     *******/

    /**
     * @param string $editTitle
     * @return $this
     */
    public function setEditTitle(string $editTitle): self
    {
        $this->editTitle = $editTitle;

        return $this;
    }

    public function getEditTitle(): string
    {
        return $this->editTitle;
    }

    public function setEditTemplate(string $editTemplate): self
    {
        $this->editTemplate = $editTemplate;

        return $this;
    }

    public function getEditTemplate(): string
    {
        return $this->editTemplate;
    }

    public function getEditForm()
    {
        if (!$this->editForm) {
            throw CrudEntityEditFormClassNotFoundException::create();
        }

        return $this->editForm;
    }

    public function setEditForm(string $editForm): self
    {
        if (!in_array(AdminFormInterface::class, class_implements($editForm))) {
            throw new \Exception('Form class must implement AdminFormInterface');
        }

        $this->editForm = $editForm;

        return $this;
    }



    /********
     * LIST *
     *******/

    public function setListSearchForm(?string $listSearchForm): self
    {
        $this->listSearchForm = $listSearchForm;

        return $this;
    }

    public function getListSearchForm(): ?string
    {
        return $this->listSearchForm;
    }

    public function setListTitle(string $listTitle): self
    {
        $this->listTitle = $listTitle;

        return $this;
    }

    public function getListTemplate(): string
    {
        return $this->listTemplate;
    }

    public function setListTemplate(string $listTemplate): self
    {
        $this->listTemplate = $listTemplate;

        return $this;
    }

    /*************
     * TEMPLATES *
     ************/

    public function getShowTemplate(): string
    {
        return $this->showTemplate;
    }

    public function setShowTemplate(string $showTemplate): self
    {
        $this->showTemplate = $showTemplate;

        return $this;
    }

    public function getListDeleteFormTemplate(): string
    {
        return $this->listDeleteFormTemplate;
    }

    public function getNewTitle(): string
    {
        return $this->newTitle;
    }

    public function getListTitle(): string
    {
        return $this->listTitle;
    }

    public function getShowTitle(): string
    {
        return $this->showTitle;
    }

    public function setListColumns(array $listColumns): self
    {
        // Get the entity properties
        $entityProperties = $this->entityMappingService->getEntityPropertiesNames($this->getEntityFqcn());
        // TODO validate array structure
        $entityProperties = array_map('strtolower', $entityProperties);

        // If the client has set a list of columns he wants to display, check if the property exist
        foreach ($listColumns as $column) {
            if (false === in_array(strtolower($column['property']), $entityProperties)) {
                // throw new \InvalidArgumentException(sprintf('Column name %s not found in entity field list.', $column['property']));
            }
        }
        $this->listColumns = $listColumns;

        return $this;
    }

    /**
     * This function is responsible for return a list column name and values.
     */
    public function getListColumns(): array
    {
        // Get the entity properties
        $entityProperties = $this->entityMappingService->getEntityPropertiesNames($this->getEntityFqcn());
        // If the client has NOT set a list of columns he wants to display, return all
        // properties as columns in the list
        if (empty($this->listColumns)) {
            foreach ($entityProperties as $property) {
                $this->listColumns[] = ['name' => $property, 'property' => $property];
            }

            return $this->listColumns;
        }

        return $this->listColumns;
    }

    public function getListColumnsCount(): int
    {
        return count($this->getListColumns());
    }

    /**
     * This function is responsible to set the rows will be shown in the show page
     * format of $shotRows [['name' => '', 'property' => 'name']]
     * @param array $showRows
     * @return $this
     */
    public function setShowRows(array $showRows): self
    {
        // Get the entity properties
        $entityProperties = $this->entityMappingService->getEntityPropertiesNames($this->getEntityFqcn());
        $entityProperties = array_map('strtolower', $entityProperties);
        // If the client has set a list of columns he wants to display, check if the property exist
        foreach ($showRows as $column) {
            if (false === in_array(strtolower($column['property']), $entityProperties)) {
                throw new \InvalidArgumentException(sprintf('Column name %s not found in entity field list.', $column));
            }
        }
        $this->showRows = $showRows;

        return $this;
    }

    public function getShowRows(): array
    {
        // Get the entity properties
        $entityProperties = $this->entityMappingService->getEntityPropertiesNames($this->getEntityFqcn());
        // If the client has NOT set a list of columns he wants to display, return all
        // properties as columns in the list
        if (empty($this->showRows)) {
            foreach ($entityProperties as $property) {
                $this->showRows[] = ['name' => $property, 'property' => $property];
            }

            return $this->showRows;
        }

        return $this->showRows;
    }

    /**
     * This function is responsible to map data between the database entity with the
     * form entity.
     * This is how symfony works by default, but in the case we are using a DTO
     * we have to map DTO information to entity.
     *
     * @return void
     *              public function mapEditFormDataClassDataToEntity($formDataClass, $entity)
     *              {
     *              //if form not using a DTO do nothing
     *              if ($formDataClass instanceof $entity) {
     *              return $entity;
     *              }
     *              //if form is using a DTO map the DTO data to the entity
     *              if (!($newFormDataClassFactory = $this->getNewFormDataClassFactoryFqcn())) {
     *              throw CrudEntityArgumentCountException::createFromClass($this->getEntityFqcn());
     *              }
     *              foreach ($this->entityMappingService->getEntityPropertiesNames($formDataClass::class) as $property) {
     *              if ($property === 'id') continue;
     *              $entity->{"set$property"}($formDataClass->$property);
     *              }
     *              return $entity;
     *              }
     */

    /**
     * Get entity properties count.
     */
    public function getEntityPropertiesCount(): int
    {
        return count($this->entityMappingService->getEntityPropertiesNames($this->getEntityFqcn()));
    }

    public function getEntityPropertyValue(string $entityFqcn, $entity, string $property): ?string
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $propertyValue = $propertyAccessor->getValue($entity, $property);

        switch ($propertyValue) {
            case is_string($propertyValue):
            case is_int($propertyValue):
                $returnedValue =  $propertyAccessor->getValue($entity, $property);
            break;
            case is_float($propertyValue):
                $returnedValue = (float) $propertyAccessor->getValue($entity, $property);
                break;
            case $propertyValue instanceof \DateTime:
            case $propertyValue instanceof \DateTimeImmutable:
                $dateTime = $propertyAccessor->getValue($entity, $property);
                $returnedValue = $dateTime->format('d-m-Y H:i');
                break;
            case $propertyValue instanceof PersistentCollection:
                $returnedValue = count($propertyAccessor->getValue($entity, $property)) . ' Items.';
                break;
            case is_object($propertyValue):
                //TODO here check if is a related class not 'an object'
                $object = $propertyAccessor->getValue($entity, $property);
                try {
                    $returnedValue = (string) $object;
                }
                catch (\Throwable $e) {
                    $returnedValue = $object->getId();
                }

                break;
            default:
                $returnedValue = '';
        }

        return $returnedValue;
    }

    public function setAllowedActions(array $allowedActions): self
    {
        $this->allowedActions = $allowedActions;

        return $this;
    }

    public function getAllowedActions(): array
    {
        return $this->allowedActions;
    }

    // it must return a FQCN (fully-qualified class name) of a Doctrine ORM entity
    abstract public function getEntityFqcn(): string;

    public static function getRouteName(): string
    {
        return '';
    }

    abstract protected function configureCrud(): void;
}
