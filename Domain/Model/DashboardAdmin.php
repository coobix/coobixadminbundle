<?php

namespace Coobix\AdminBundle\Domain\Model;

class DashboardAdmin extends Admin
{
    protected function configureCrud(): void
    {
        $this->setShowTemplate('@CoobixAdmin/metronic/Admin/dashboard.html.twig');
    }

    public function getEntityFqcn(): string
    {
        return '';
    }

    public static function getRouteName(): string
    {
        return 'dashboard';
    }
}
