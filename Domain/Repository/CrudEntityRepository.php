<?php

namespace Coobix\AdminBundle\Domain\Repository;

interface CrudEntityRepository
{
    public function findOneById(string $classFqcn, string $id): object|null;

    public function save(object $crudEntity): void;
}
